package com.fertiletech.sap.apply;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

public class WelcomeJumper extends Composite{
	String title = "<H1>Get Started</H1>\n<br/>";
	HTML display = new HTML();
	Anchor staticJump = new Anchor("Click here to get started if you're not redirected" +
			" in a few seconds", GWT.getHostPageBaseURL());
	
	WelcomeJumper()
	{
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(20);
		container.add(display);
		container.add(staticJump);
		initWidget(container);
		
	}
	
	public void setHTML(String msg)
	{
		display.setHTML(title + msg);
		History.newItem(NameTokens.WELCOME);
	}

}
