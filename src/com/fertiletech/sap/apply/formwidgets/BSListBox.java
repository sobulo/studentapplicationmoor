package com.fertiletech.sap.apply.formwidgets;


import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.base.HasPlaceholder;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;

public class BSListBox extends ListBox implements HasValue<String>, HasPlaceholder{
	
	public BSListBox() {
		super();
		addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				fireMyEvents();
			}
		});
	}
	
	private void fireMyEvents()
	{
		ValueChangeEvent.fire(BSListBox.this, getValue());
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	@Override
	public void setValue(String value) {
		for(int i = 0; i < getItemCount(); i++)
			if(getValue(i).equalsIgnoreCase(value))
			{
				setSelectedIndex(i);
				break;
			}
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		setValue(value);
		if(fireEvents)
			fireMyEvents();
	}

	@Override
	public void setPlaceholder(String placeholder) {
		this.setTitle(placeholder);
	}

	@Override
	public String getPlaceholder() {
		return this.getTitle();
	}

	@Override
	public String getValue() {
		if(getItemCount() == 0)
			return null;
		
		return getValue(getSelectedIndex());
	}
}

