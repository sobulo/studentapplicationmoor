/**
 * 
 */
package com.fertiletech.sap.apply.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{
	
	@Source("logo.png")
	ImageResource logoSmall();

	@Source("animal.png")
	ImageResource thumb1();
	@Source("fisheries.png")
	ImageResource thumb2();	
	@Source("statistics.png")
	ImageResource thumb3();
	@Source("com.png")
	ImageResource thumb5();
	@Source("slt.png")
	ImageResource thumb6();
	@Source("aem.png")
	ImageResource thumb7();
	@Source("anh.png")
	ImageResource thumb8();
	@Source("apta.png")
	ImageResource thumb4();

	@Source("animal2.png")
	ImageResource thumb1b();
	@Source("fisheries2.png")
	ImageResource thumb2b();	
	@Source("statistics2.png")
	ImageResource thumb3b();
	@Source("comb.png")
	ImageResource thumb5b();
	@Source("slt2.png")
	ImageResource thumb6b();
	@Source("aem2.png")
	ImageResource thumb7b();
	@Source("anh2.png")
	ImageResource thumb8b();
	@Source("apta2.png")
	ImageResource thumb4b();

	@Source("earn.jpg")
	ImageResource home();
	@Source("servingyou.jpg")
	ImageResource homeb();
	@Source("thankside.png")	
	ImageResource homec();
	
}
