package com.fertiletech.sap.apply;

import org.gwtbootstrap3.client.ui.Jumbotron;

import com.fertiletech.sap.apply.forms.GetStartedForm;
import com.fertiletech.sap.apply.forms.MapBoard;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class WelcomePage extends Composite implements HyperlinkedPanel{
	@UiField
	Jumbotron content;
	GetStartedForm miniApplyForm;
	MapBoard opsDisplay;
	private static WelcomePageUiBinder uiBinder = GWT
			.create(WelcomePageUiBinder.class);	
	
	interface WelcomePageUiBinder extends UiBinder<Widget, WelcomePage> {
	}

	public WelcomePage() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Hyperlink getLink() {
		return new Hyperlink("Welcome", NameTokens.WELCOME);
	}

	@Override
	public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
		content.clear();
		if(isOps)
		{
			if(opsDisplay == null)
				opsDisplay = new MapBoard();
			opsDisplay.update(CustomerAppHelper.getLoanID(args));
			content.add(opsDisplay);
		}
		else
		{
			if(miniApplyForm == null)
				miniApplyForm = new GetStartedForm();
			miniApplyForm.updateForm(args, isOps, isLoggedIn);
			content.add(miniApplyForm);
		}
		return this;
	}
	
}
