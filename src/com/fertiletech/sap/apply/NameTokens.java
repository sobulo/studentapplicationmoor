package com.fertiletech.sap.apply;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.user.datepicker.client.CalendarUtil;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * @author Joshua Godi
 */
public class NameTokens {
    // General Pages
    public static final String WELCOME = "welcome";
    public static final String APPLY = "apply";
    public static final String PRINT = "print";
    public static final String UPLOAD = "attach";
    public static final String STATUS = "status";
    public static final String CALCULATOR = "calculator";

    private final static String URL1 = "http://www.fcahptib.edu.ng/admission/national-diploma-in-animal-health-and-production-technology";
    private final static String URL2 = "http://www.fcahptib.edu.ng/admission/national-diploma-in-fisheries-technology";
    private final static String URL3 = "http://www.fcahptib.edu.ng/admission/national-diploma-in-statistics";
    private final static String URL4 = "http://www.fcahptib.edu.ng/admission/higher-national-diploma-in-animal-production-technology";    
    private final static String URL5 = "http://www.fcahptib.edu.ng/admission/national-diploma-in-computer-science";
    private final static String URL6 = "http://www.fcahptib.edu.ng/admission/national-diploma-in-science-laboratory-technology";
    private final static String URL7 = "http://www.fcahptib.edu.ng/admission/higher-national-diploma-in-agricultural-extension-and-management";
    private final static String URL8 = "http://www.fcahptib.edu.ng/admission/higher-national-diploma-in-animal-health-technology";
    private final static String URL9 = "http://www.fcahptib.edu.ng/contact-us";
    private final static String URL10 = "http://www.fcahptib.edu.ng/About-FCAHPT";
    private final static String URL11 = "http://http://www.fcahptib.edu.ng/admission";
   
    // Getters for UiBinders
    public final static HashMap<String, String> getPropertyNameMap()
    {
    	HashMap<String, String> result = new LinkedHashMap<String, String>();
    	result.put(URL1, "Animal Health & Production Tech. - ND");
    	result.put(URL2, "Fisheries - ND");
    	result.put(URL3, "Statistics - ND");
    	result.put(URL5, "Computer Science - ND");
    	result.put(URL6, "Science Laboratory Technology - ND");
    	result.put(URL7, "Agric Extension Management - HND");
    	result.put(URL8, "Animal Health Technology - HND");
    	result.put(URL4, "Animal Production Technology - HND");
    	return result;
    }
    
    public final static HashMap<String, String> getAboutUsMap()
    {
    	LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
    	result.put(URL9, "General Enquiries");
    	@SuppressWarnings("deprecation")
		Date d = new Date(64, 4, 1);
    	int days = CalendarUtil.getDaysBetween(d, new Date());
    	int years = Math.round(days/366.0f);
    	result.put(URL10, "Over " + years + " years of excellence");
    	result.put(URL11, "College Admissions");
    	return result;
    }    
    
    public final static HashMap<String, IconType> getAboutIcons()
    {
    	LinkedHashMap<String, IconType> result = new LinkedHashMap<String, IconType>();
    	result.put(URL9, IconType.PHONE_SQUARE);
    	result.put(URL10, IconType.TROPHY);
    	result.put(URL11, IconType.GAVEL);
    	return result;
    }

	public static String getUrl1() {
		return URL1;
	}

	public static String getUrl2() {
		return URL2;
	}

	public static String getUrl3() {
		return URL3;
	}

	public static String getUrl4() {
		return URL4;
	}

	public static String getUrl5() {
		return URL5;
	}

	public static String getUrl6() {
		return URL6;
	}

	public static String getUrl7() {
		return URL7;
	}

	public static String getUrl8() {
		return URL8;
	}
	
	public static String getWelcome() {
		return WELCOME;
	}
}
