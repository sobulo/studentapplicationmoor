package com.fertiletech.sap.apply.forms;


import com.fertiletech.sap.apply.formwidgets.BSListBox;
import com.fertiletech.sap.apply.formwidgets.BSTextWidget;
import com.fertiletech.sap.apply.formwidgets.CurrencyBox;
import com.fertiletech.sap.apply.formwidgets.FlexPanel;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

public class FormUtils {
	public static void setupFlexPanel(FlexPanel panel, String[][] headers, Integer noRows)
	{
		TableMessageHeader h = new TableMessageHeader(headers[0].length);
		for(int i = 0; i < headers[0].length; i++)
		{
			h.setText(i, headers[0][i], TableMessageContent.TEXT);
			int width = Integer.valueOf(headers[1][i]);
			h.setFormatOption(i, String.valueOf((width-1)));
		}
		panel.initHeader(h, headers[2][0], headers[2][1], noRows);
	}

	public static void setupListBox(BSTextWidget<String> lbWrapper, String[] options) {
		BSListBox tempBox = (BSListBox) lbWrapper.getFieldWidget();
		setupListBox(tempBox, options);
	}
	
	
	public static void setupListBox(BSListBox tempBox, String[] options) {
		if(tempBox.getItemCount() > 0)
			tempBox.clear();
		for (int counter = 0; counter < options.length; counter++) {
			tempBox.addItem(options[counter]);
		}
	}
	
	public static Double getAmount(BSTextWidget<String> currencyBox) {
		CurrencyBox cb = (CurrencyBox) currencyBox.getFieldWidget();
		return cb.getAmount();
	}	
	
	
	public static void setupListBox(BSListBox lb, int start, int finish, String singular, String plural)
	{	
		for(int i = start; i <= finish ; i++)
		{
			if(i == 1)
				lb.addItem(i + " " + singular, String.valueOf(i));
			else
				lb.addItem(i + " " + plural, String.valueOf(i));
		}
	}
	
	public static void setupListBox(BSTextWidget<String> lb, int start, int finish, String singular, String plural)
	{
		BSListBox box = (BSListBox) lb.getFieldWidget();
		setupListBox(box, start, finish, singular, plural);
	}
	
	public static void setOtherListCombo(BSTextWidget<String> lb, BSTextWidget<String> otherField, String value)
	{
		BSListBox box = (BSListBox) lb.getFieldWidget();
		setOtherListCombo(box, otherField, value);
	}	
	
	public static void setOtherListCombo(BSListBox lb, BSTextWidget<String> otherField, String value)
	{
		if(value == null)
		{
			otherField.clear();
			otherField.setEnabled(false);
			lb.setSelectedIndex(0);
			return;
		}
		
		boolean useOther = true;
		for(int i = 0; i < lb.getItemCount() - 1; i++)
			if(lb.getValue(i).equalsIgnoreCase(value))
				useOther = false;
		
		if(!useOther)
		{
			lb.setValue(value);
			otherField.clear();
			otherField.setEnabled(false);
		}
		else
		{
			lb.setSelectedIndex(lb.getItemCount() - 1);
			otherField.setValue(value);
			otherField.setEnabled(true);
		}
			
	}
	
	public static void initOtherListCombo(BSTextWidget<String> lb,BSTextWidget<String> otherField)
	{
		initOtherListCombo((BSListBox) lb.getFieldWidget(), otherField);
	}
	
	public static void initOtherListCombo(final BSListBox lb,final BSTextWidget<String> otherField)
	{
		lb.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if(event.getValue().equals(lb.getValue(lb.getItemCount() - 1)))
				{
					otherField.setEnabled(true);
				}
				else
				{
					otherField.clear();
					otherField.setEnabled(false);
				}			
			}
		});
	}
	
	public static String getFullName(String[] nameParts)
	{
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < nameParts.length; i++)
			if(nameParts[i] == null)
				continue;
			else
				result.append(nameParts[i].trim()).append(" ");
		return result.toString().trim();
	}	
}
