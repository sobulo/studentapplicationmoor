package com.fertiletech.sap.apply.forms;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.apply.formwidgets.BSLoanFieldValidator;
import com.fertiletech.sap.apply.formwidgets.BSTextWidget;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.FormValidator;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.fertiletech.sap.shared.oauth.ClientUtils.NPMBUserCookie;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class GetStartedForm extends Composite{
	@UiField
	BSTextWidget<String> name;
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> surname;
	@UiField
	Button apply;
	
	private static GetStartedFormUiBinder uiBinder = GWT
			.create(GetStartedFormUiBinder.class);

	interface GetStartedFormUiBinder extends UiBinder<Widget, GetStartedForm> {
	}

	protected AsyncCallback<String[]> applynowCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			String msg = "Error processing application. ";
			if(caught instanceof DuplicateEntitiesException)
				msg = "Looks like you already applied. Try logging in to continue your application or contact customer service for assistance. ";
			else
			{
				apply.setEnabled(true);
				enableMiniApplyForm(true);
			}
			CustomerAppHelper.showErrorMessage(msg + caught.getMessage());
			apply.setEnabled(true);
			loanID = null;
			
		}

		@Override
		public void onSuccess(String[] result) {
			CustomerAppHelper.showInfoMessage("An application file has been created for you.");
			loanID = result[0];
			CustomerAppHelper.updateLatLngVals(loanID);
			History.newItem(NameTokens.APPLY + "/" + loanID); //go to loan form
		}
	};		
	
	public GetStartedForm() {
		initWidget(uiBinder.createAndBindUi(this));
		apply.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				BSTextWidget<String>[] fields = new BSTextWidget[4];
				fields[0] = email;
				fields[1] = phone;
				fields[2] = name;
				fields[3] = surname;
				String[] formFields = {NPMBFormConstants.EMAIL, NPMBFormConstants.TEL_NO, NPMBFormConstants.FIRST_NAME, NPMBFormConstants.SURNAME};
				
				
				
				HashMap<String, String> errorMap = new HashMap<String, String>();
				for(int i = 0; i < formFields.length; i++)
				{
					String formKey = formFields[i];
					FormValidator[] oldStyleValidator = NPMBFormConstants.WELCOME_VALIDATORS.get(formKey);
					BSLoanFieldValidator validator = new BSLoanFieldValidator(oldStyleValidator, fields[i], formKey, errorMap, true);
					validator.markErrors();
				}
				
				if(errorMap.size() > 0)
					CustomerAppHelper.showErrorMessage(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found. See highlighted fields.");
				else
				{
					if(loanID != null)
					{
						
						History.newItem(NameTokens.APPLY + "/" + loanID);
						return;
					}
					enableMiniApplyForm(false);
					apply.setEnabled(false);
					CustomerAppHelper.LOAN_MKT_SERVICE.startLoanApplication( getFormData(), applynowCallback);
				}
			}			
		});		
		
	}

	String loanID;
	private AsyncCallback<HashMap<String, String>> loadCallback = new AsyncCallback<HashMap<String,String>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage("Error reaching server. Try refreshing your broweser." + caught.getMessage());
			apply.setEnabled(true);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			if(email.getValue().trim().length() > 0)
			{
				if(!email.getValue().trim().equalsIgnoreCase(result.get(NPMBFormConstants.EMAIL)))
				{
					History.newItem(NameTokens.WELCOME); //blank out param arg in url
					return;
				}
			}
			setFormData(result);
			apply.setText("Continue Application");
			loanID = result.get(NPMBFormConstants.ID);
			apply.setEnabled(true);
		}
	};
	
	public void enableMiniApplyForm(boolean enabled)
	{
		email.setEnabled(enabled);
		phone.setEnabled(enabled);
		surname.setEnabled(enabled);
		name.setEnabled(enabled);
		
	}
	
	public void setAndLockEmail(String address)
	{
		email.setValue(address);
		email.setEnabled(false);
	}
	
	public void setName(String myName)
	{
		name.setValue(myName);
	}
	
	public void clear()
	{
		setFormData(new HashMap<String, String>());
		apply.setText("Get Started");
	}
	
	public void setFormData(HashMap<String, String> data)
	{
		email.setValue(data.get(NPMBFormConstants.EMAIL));
		phone.setValue(data.get(NPMBFormConstants.TEL_NO));
		surname.setValue(data.get(NPMBFormConstants.SURNAME));
		name.setValue(data.get(NPMBFormConstants.FIRST_NAME));
	}
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(NPMBFormConstants.EMAIL, email.getValue());
		result.put(NPMBFormConstants.TEL_NO, phone.getValue());
		result.put(NPMBFormConstants.FIRST_NAME, name.getValue());
		result.put(NPMBFormConstants.SURNAME, surname.getValue());
		return result;
	}
	

	
	public String[] getNameParts(String fullName)
	{
		String[] parts = fullName.split(" ");
		for(int i = 0; i < parts.length; i++)
			parts[i] = parts[i].trim();
		String[] result = new String[3];
		if(parts.length == 1)
			result[0] = parts[0];
		else if( parts.length == 2)
		{
			result[0] = parts[1];
			result[1] = parts[0];
		}
		else if(parts.length > 2)
		{
			result[0] = parts[2];
			result[1] = parts[0];
			result[2] = parts[1];
		}
		return result;
	}
	
	public void enableApplyButton(boolean enabled)
	{
		apply.setEnabled(enabled);
	}
	
	public void updateForm(String[] args, boolean isOps, boolean isLoggedIn)
	{
		String id = CustomerAppHelper.getLoanID(args);
		clear();
		
		if(id != null)
		{
			apply.setEnabled(false);
			enableMiniApplyForm(false);
			CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(id, loadCallback);
		}
		
		if(isLoggedIn && !isOps)
		{
			NPMBUserCookie uc = NPMBUserCookie.getCookie();
			setAndLockEmail(uc.getEmail());
			String myname = uc.getUserName();
			if(myname != null && myname.length() > 0)
			{
				String[] nameParts = myname.trim().split(" ");
				name.setValue(nameParts[0]);
				if(nameParts.length > 1)
					surname.setValue(nameParts[nameParts.length - 1]);
			}
			else
				name.setValue(myname);
		}
	}

}
