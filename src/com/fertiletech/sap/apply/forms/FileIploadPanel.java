package com.fertiletech.sap.apply.forms;

import java.util.List;

import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;
import org.gwtbootstrap3.extras.bootbox.client.callback.ConfirmCallback;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.table.TablesView;
import com.fertiletech.sap.client.MyAsyncCallback;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Widget;

public class FileIploadPanel extends Composite implements ClickHandler,
		FormPanel.SubmitCompleteHandler {

	@UiField
	Button submit;
	@UiField
	Button refresh;
	@UiField
	FileUpload selectFile;
	@UiField
	FormPanel uploadForm;
	//@UiField
	//HTML status;
	@UiField
	TablesView fileList;
	@UiField
	Hidden loanID;
	@UiField
	Paragraph finePrint;
	private static FileIploadPanelUiBinder uiBinder = GWT
			.create(FileIploadPanelUiBinder.class);

	interface FileIploadPanelUiBinder extends UiBinder<Widget, FileIploadPanel> {
	}

	AsyncCallback<List<TableMessage>> refreshCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper
					.showErrorMessage("File list refresh failed. Error was: "
							+ caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			boolean alreadyInitialized = fileList.isInitialized();
			fileList.showTable(result);
			if (!alreadyInitialized) {
				fileList.addColumn(getColumn(), "View");
				fileList.addColumn(getDeleteColumn(), "Delete");
			}
			CustomerAppHelper
					.showInfoMessage("File list refreshed");
		}
	};	
	
	public FileIploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		submit.addClickHandler(this);
		uploadForm.addSubmitCompleteHandler(this);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		selectFile.setName("chooseFile");
		enableAll(false);
		
		refresh.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
				{
					CustomerAppHelper.showErrorMessage("No application selected. Aborting refresh");
					return;
				}				
				CustomerAppHelper.READ_SERVICE.getUploads(loanID.getValue(), refreshCallback);
			}
		});
	}

	private void enableAll(boolean enabled) {
		selectFile.setEnabled(enabled);
		refresh.setEnabled(enabled);
		submit.setEnabled(enabled);
	}

	private void clear() {
		fileList.clear();
		loanID.setName("dummy");
		loanID.setValue("");
		enableAll(false);
	}

	private void setFinePrint(String style, String message)
	{
		finePrint.setHTML("<span style='" + "'>" + message + "</span>");		
	}
	
	public void setLoanID(String newLoanID, boolean isOps, boolean isLoggedIn) {
		clear();
		if(!isLoggedIn)
		{
			String message = "Uploads disabled. Please sign in.";
			setFinePrint("color:purple", message);
			CustomerAppHelper.showErrorMessage(message);
			return;
		}
		if (newLoanID == null || newLoanID.trim().length() == 0)
		{
			String msg = "Uploads disabled. " + (isOps? "Select an application on M.A.P." : " Try refreshing your browser");
			CustomerAppHelper.showErrorMessage(msg);
			setFinePrint("color:red", msg);
			return;
		}
		if(isOps)
		{
			setFinePrint("background-color: purple; color:white;", "NOTEWORTHY: Uploads done here are visible to the applicant." +
					" Use the Drive picker on M.A.P. status page to attach files that are private to College staff. " +
					" You can also choose to share such files with only members of your team.");
		}
	
		loanID.setName(DTOConstants.GCS_LOAN_ID_PARAM);
		loanID.setValue(newLoanID);
		enableAll(true);
	}

	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		CustomerAppHelper.READ_SERVICE.getUploads(loanID.getValue(), refreshCallback);
		CustomerAppHelper.showInfoMessage("Upload Successful!");
	}

	@Override
	public void onClick(ClickEvent event) {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper
						.showErrorMessage("Uploads disabled, error message is: "
								+ caught.getMessage());
				enableAll(true);
			}

			@Override
			public void onSuccess(String result) {
				if (result == null)
					return;
				
				uploadForm.setAction(result);
				enableAll(true);
				uploadForm.submit();
			}
		};
		if(selectFile.getFilename() == null || selectFile.getFilename().trim().length()==0)
		{
			CustomerAppHelper.showErrorMessage("Select a file to upload");
			return;
		}
		enableAll(false);
		if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
		{
			CustomerAppHelper.showErrorMessage("No application selected. Aborting upload");
			return;
		}
		CustomerAppHelper.READ_SERVICE.getUploadUrl(callback);
	}

	private Column<TableMessage, SafeHtml> getColumn() {
		SafeHtmlCell cell = new SafeHtmlCell();
		Column<TableMessage, SafeHtml> urlColumn = new Column<TableMessage, SafeHtml>(
				cell) {

			@Override
			public SafeHtml getValue(TableMessage object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				String url = object.getMessageId();
				sb.appendHtmlConstant("<a target='_blank' href='" + url
						+ "'>Download</a>");
				return sb.toSafeHtml();
			}
		};
		return urlColumn;
	}
	
	private Column<TableMessage, String> getDeleteColumn()
	{
		ButtonCell removeAttachment = new ButtonCell();
		Column<TableMessage, String> invCol = new Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "Delete File";
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {
				final String[] parts = object.getText(1).split("/", 2);
				
				final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Looks like attachment removal failed. " 
									+ caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						CustomerAppHelper.READ_SERVICE.getUploads(parts[0], refreshCallback);

					}

					@Override
					protected void callService(AsyncCallback<String> cb) {
						enableWarning(false);
						CustomerAppHelper.READ_SERVICE.deleteUpload(object.getText(1), cb);
					}
				};
				
				Bootbox.confirm("The file" + ", " + object.getText(0) + " will be permanently deleted from New Prudential Records. Proceed?", new ConfirmCallback() {
					
					@Override
					public void callback(boolean result) {
						if(result)
							editAttachCallback.go("Deleting file " + parts[1]);
						else
							CustomerAppHelper.showInfoMessage("Aborted delete of " + parts[1]);
					}
				});
			}
		});
		return invCol;
	}
}
