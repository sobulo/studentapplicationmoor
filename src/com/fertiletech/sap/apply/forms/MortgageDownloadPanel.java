package com.fertiletech.sap.apply.forms;

import org.gwtbootstrap3.client.ui.AnchorButton;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MortgageDownloadPanel extends Composite {

	@UiField
	AnchorButton downloadLink;
	@UiField
	AnchorButton checklistLink;
	
	private static MortgageDownloadPanelUiBinder uiBinder = GWT
			.create(MortgageDownloadPanelUiBinder.class);

	interface MortgageDownloadPanelUiBinder extends
			UiBinder<Widget, MortgageDownloadPanel> {
	}
	ClickHandler warningHandler = null;
	HandlerRegistration appWarnRegistration = null;
	HandlerRegistration listWarnRegistration = null;
	private final String WAIT_MESSAGE = "Fetching link, wait a few seconds and try again";
	private final String NOT_READY_MESSAGE = "You have NOT yet completed/submitted your online application";
	private final String ERROR_MESSAGE = "Failed to fetch download link. Try refreshing your browser.";
	private final String NO_ID_MESSAGE = "No application id specified. Visit the home page to start your application";
	private final String NO_ID_OPS_MESSAGE = "No application id specified. Contact IT to find out how to use M.A.P to access applications submitted online.";
	
	String warningMessage = WAIT_MESSAGE;
	private AsyncCallback<String[]> appDownloadLinkCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			warningMessage = ERROR_MESSAGE;
			Window.alert(warningMessage + " " + caught.getMessage());
		}

		@Override
		public void onSuccess(String[] result) {
			clearHandlers();
			downloadLink.setHref(result[0]);
			checklistLink.setHref(result[1]);
			downloadLink.setType(ButtonType.SUCCESS);
			checklistLink.setType(ButtonType.INFO);
		}
	};
	
	public MortgageDownloadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		warningHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				CustomerAppHelper.showErrorMessage(warningMessage);
			}
		};
		downloadLink.setEnabled(false);
	}
	
	public void clear()
	{
		clearHandlers();
		String disableLink = "javascript:;";
		appWarnRegistration = downloadLink.addClickHandler(warningHandler);
		listWarnRegistration = checklistLink.addClickHandler(warningHandler);
		downloadLink.setHref(disableLink);
		checklistLink.setHref(disableLink);
		downloadLink.setType(ButtonType.DEFAULT);
		checklistLink.setType(ButtonType.DEFAULT);
	}
	
	public void setLoanID(String id, boolean isOps, boolean isLoggedIn)
	{
		clear();
		if(id == null)
		{
			if(isOps)
				warningMessage = NO_ID_OPS_MESSAGE;
			else
			{
				warningMessage = NO_ID_MESSAGE;
				if(!isLoggedIn)
					warningMessage += " or login to access an existing application";
			}
			return;
		}
		warningMessage = WAIT_MESSAGE;
		AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

			@Override
			public void onFailure(Throwable caught) {
				appDownloadLinkCallback.onFailure(caught);
			}

			@Override
			public void onSuccess(TableMessage result) {
				Boolean submitted = NPMBFormConstants.getSubmissionValues(result)[0];
				if(submitted)
					CustomerAppHelper.LOAN_MKT_SERVICE.getApplicationFormDownloadLink(result.getText(DTOConstants.LOAN_KEY_IDX), 
							appDownloadLinkCallback );
				else
				{
					clear();
					warningMessage = NOT_READY_MESSAGE;					
				}
			}
		};
		CustomerAppHelper.LOAN_MKT_SERVICE.getLoanState(id, callback );
		
	}
	
	private void clearHandlers()
	{
		if(appWarnRegistration != null)
			appWarnRegistration.removeHandler();
		if(listWarnRegistration != null)
			listWarnRegistration.removeHandler();
		appWarnRegistration = null;
		listWarnRegistration = null;
	}
}
