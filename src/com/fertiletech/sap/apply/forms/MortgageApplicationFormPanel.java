package com.fertiletech.sap.apply.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TabContent;
import org.gwtbootstrap3.client.ui.TabListItem;
import org.gwtbootstrap3.client.ui.TabPane;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;
import org.gwtbootstrap3.extras.bootbox.client.callback.ConfirmCallback;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.apply.formwidgets.BSControlValidator;
import com.fertiletech.sap.apply.formwidgets.BSDatePicker;
import com.fertiletech.sap.apply.formwidgets.BSLoanFieldValidator;
import com.fertiletech.sap.apply.formwidgets.BSTextWidget;
import com.fertiletech.sap.apply.formwidgets.CurrencyBox;
import com.fertiletech.sap.apply.formwidgets.FlexPanel;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.FormValidator;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class MortgageApplicationFormPanel extends Composite{

	@UiField
	TabListItem personalHeader;
	@UiField
	TabListItem educationHeader;
	@UiField
	TabListItem familyHeader;
	@UiField
	TabListItem signatureHeader;
	@UiField
	TabPane personalBody;
	@UiField
	TabPane educationBody;
	@UiField
	TabPane familyBody;
	@UiField
	TabPane signatureBody;
	
	@UiField
	FlexPanel guardianSlot;
	@UiField
	FlexPanel olevOneSlot;
	@UiField
	FlexPanel olevTwoSlot;
	
	@UiField
	BSTextWidget<String> surname;
	@UiField
	BSTextWidget<String> firstName;
	@UiField
	BSTextWidget<String> middleName;
	@UiField
	BSTextWidget<String> residentialAddress;
	@UiField
	BSTextWidget<String> spouseAddress;
	@UiField
	BSTextWidget<String> mailingAddress;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> alternateNo;	
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<Date> dob;
	@UiField
	BSTextWidget<String> pob;
	@UiField
	BSTextWidget<String> gender;
	@UiField
	BSTextWidget<String> nationality;
	@UiField
	BSTextWidget<String> origin;
	@UiField
	BSTextWidget<String> lga;	
	
	
	
	
	
	
	@UiField
	BSTextWidget<String> maritalStatus;
	@UiField
	BSTextWidget<String> spouseName;
	@UiField
	BSTextWidget<String> spousePhone;
	@UiField
	BSTextWidget<String> spouseEmail;
	@UiField
	Paragraph declarationContent;
	@UiField
	Small declarationAuthor;
	
	@UiField
	BSTextWidget<String> sessType;
	@UiField
	BSTextWidget<String> diplomaType;
	@UiField
	BSTextWidget<String> departmentType;
	@UiField
	BSTextWidget<String> jambNo;
	@UiField
	BSTextWidget<String> jambScore;
	@UiField
	BSTextWidget<String> jambYear;
	@UiField
	BSTextWidget<String> olevOneName;
	@UiField
	BSTextWidget<String> olevOneCredits;
	@UiField
	BSTextWidget<String> olevOneYear;
	@UiField
	BSTextWidget<String> olevTwoName;
	@UiField
	BSTextWidget<String> olevTwoYear;
	@UiField
	BSTextWidget<String> olevTwoCredits;
	@UiField
	BSTextWidget<String> priorSchool;
	@UiField
	BSTextWidget<String> priorDiploma;
	@UiField
	BSTextWidget<String> priorYear;	
	@UiField
	BSTextWidget<String> priorGPA;
	@UiField
	BSTextWidget<String> priorMaxGPA;
	@UiField
	BSTextWidget<String> priorSessType;
	
	
	@UiField
	BSTextWidget<String> itName;
	@UiField
	BSTextWidget<String> itAddress;
	@UiField
	BSTextWidget<String> itPhone;	
	@UiField
	BSTextWidget<String> itStart;
	@UiField
	BSTextWidget<String> itEnd;
	
	
	@UiField
	Button personalSave;
	@UiField
	Button familySave;	
	@UiField
	Button educationSave;
	@UiField
	Button submit;
		
	String loanId = null;
	
	HashMap<Button, WorkflowStateInstance> buttonToStateMap = new HashMap<Button, WorkflowStateInstance>();
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(final HashMap<String, String> result) {
        	final String setEmail = email.getValue();
        	try
        	{
        		setFormData(result); 
        	}
        	catch(Exception e)
        	{
        		StringBuilder errorMessage = new StringBuilder("<b>Warning, a few fields might not have " +
        				"been loaded: " + e.getMessage() + "<b><hr><div style='font-size:smaller'><ul>");
        		for(String key :result.keySet())
        			errorMessage.append("<li>").append(result.get(key)).append("</li>");
        		errorMessage.append("</ul></div>");
				CustomerAppHelper.showErrorMessage(errorMessage.toString());
				
        	}
        	CustomerAppHelper.LOAN_MKT_SERVICE.getSupplementaryData(result.get(NPMBFormConstants.ID), new AsyncCallback<ArrayList<HashMap<String,String>>[]>() {

				@Override
				public void onFailure(Throwable caught) {
					fetchFormDataCallback.onFailure(caught);
				}

				@Override
				public void onSuccess(ArrayList<HashMap<String, String>>[] tables) {
					try
					{
					loanId = result.get(NPMBFormConstants.ID);
					boolean editable = Boolean.valueOf(result.get(NPMBFormConstants.ID_EDIT));
					setFormTables(tables);
					WorkflowStateInstance state = WorkflowStateInstance.valueOf(result.get(NPMBFormConstants.ID_STATE));
					enableTabs(state);
					activatePanel(state);
					//override editable, if an email set (assumption is this email set only if NOT ops/staff)
					if(editable)
						if(setEmail.trim().length() > 0 && !setEmail.equalsIgnoreCase(result.get(NPMBFormConstants.EMAIL)))
						{
							CustomerAppHelper.showInfoMessage("This app was created from a different email address, setting to read only mode. Login with " + setEmail +
									" and/or logout to edit the application form");
							editable = false;
						}
					enableEditing(editable);
					}
					catch(Exception e)
					{
						Window.alert("Warning, some table rows might not have been loaded: " + e.getMessage());
					}
				}
			});
        }

        @Override
        public void onFailure(Throwable caught) {
        	CustomerAppHelper.showErrorMessage("Unable to retrieve application form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        	enableButtons(false);
        }
    };  

	private AsyncCallback<String[]> submitCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	Bootbox.alert("Unable to submit application, please try again. Error was: " + caught.getMessage());
        	enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
        	Bootbox.alert("You application form has been submitted. If you need to make edits to the submitted application, please contact support");
        	boolean editable = Boolean.valueOf(result[DTOConstants.LOAN_EDIT_IDX]);
        	enableEditing(editable);
        	nextPanel(WorkflowStateInstance.APPLICATION_CONFIRM);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};

	private AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	Bootbox.alert("Unable to save values. Error was: " + caught.getMessage());
        	enableButtons(true);			
		}

		@Override
		public void onSuccess(String[] result) {
        	Bootbox.alert("Saved successfully. You may continue editing your application");
        	enableButtons(true);
        	WorkflowStateInstance state = WorkflowStateInstance.valueOf(result[DTOConstants.LOAN_STATE_IDX]);
        	nextPanel(state);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};
	
	private static MortgageApplicationFormPanelUiBinder uiBinder = GWT
			.create(MortgageApplicationFormPanelUiBinder.class);

	interface MortgageApplicationFormPanelUiBinder extends
			UiBinder<Widget, MortgageApplicationFormPanel> {
	}

	public MortgageApplicationFormPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		buttonToStateMap.put(personalSave, WorkflowStateInstance.APPLICATION_PERSONAL);
		buttonToStateMap.put(familySave, WorkflowStateInstance.APPLICATION_FAMILY);
		buttonToStateMap.put(submit, WorkflowStateInstance.APPLICATION_CONFIRM);
		buttonToStateMap.put(educationSave, WorkflowStateInstance.APPLICATION_EDUCATION);
		FormUtils.setupFlexPanel(guardianSlot,NPMBFormConstants.PARENT_TABLE_CFG, 3);
		FormUtils.setupFlexPanel(olevOneSlot, NPMBFormConstants.OLEV_TABLE_CFG, 11);
		FormUtils.setupFlexPanel(olevTwoSlot, NPMBFormConstants.OLEV_TABLE2_CFG, 11);
		FormUtils.setupListBox(maritalStatus, NPMBFormConstants.MARITAL_STATUS_LIST);
		FormUtils.setupListBox(gender, NPMBFormConstants.GENDER_LIST);
		FormUtils.setupListBox(olevOneName, NPMBFormConstants.O_LEV_NAME_LIST);
		FormUtils.setupListBox(olevTwoName, NPMBFormConstants.O_LEV_NAME_LIST2);
		FormUtils.setupListBox(diplomaType, NPMBFormConstants.DIPLOMA_NAME_LIST);
		FormUtils.setupListBox(departmentType, NPMBFormConstants.ND_DEPARTMENT_LIST);
		FormUtils.setupListBox(sessType, NPMBFormConstants.ADM_TYPE_LIST);
		FormUtils.setupListBox(priorSessType, NPMBFormConstants.ADM_TYPE_LIST);
		FormUtils.setupListBox(priorDiploma, NPMBFormConstants.PRIOR_DIPLOMA_NAME_LIST);
		olevTwoName.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if(event.getValue().equals(NPMBFormConstants.O_LEV_NAME_LIST2[0]))
				{
					olevTwoSlot.clear();
					olevTwoSlot.setEnabled(false);
				}
				else
					olevTwoSlot.setEnabled(true);
			}
		});
		olevTwoSlot.setEnabled(false);
		final BSTextWidget<String>[] ndFields = new BSTextWidget[3];
		ndFields[0] = jambNo; ndFields[1] = jambScore; ndFields[2] = jambYear; 
		sessType.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if(DTOConstants.FT.equals(event.getValue()) && DTOConstants.ND.equals(diplomaType.getValue()))
					clearAndEnable(true, ndFields);
				else
					clearAndEnable(false, ndFields);
				
			}
		});
		diplomaType.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String[] options = null;
				BSTextWidget<String>[] hndFields = new BSTextWidget[10];

				hndFields[0] = priorMaxGPA; hndFields[1] = priorGPA; hndFields[2] = priorSchool; hndFields[3] = priorYear; hndFields[4] = priorSessType;
				hndFields[5] = itAddress; hndFields[6] = itStart; hndFields[7] = itEnd; hndFields[8] = itName; hndFields[9] = itPhone;
				
				if(event.getValue().equals(DTOConstants.ND))
				{
					options = NPMBFormConstants.ND_DEPARTMENT_LIST;
					priorDiploma.setValue(NPMBFormConstants.PRIOR_DIPLOMA_NAME_LIST[0]);
					priorDiploma.setEnabled(false);
					clearAndEnable(false, hndFields);
					if(DTOConstants.FT.equals(sessType.getValue()))
						clearAndEnable(true, ndFields);
					educationValidators.clear();
			    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_EDUCATION_VALIDATORS, educationValidators);

				}
				else
				{
					options = NPMBFormConstants.HND_DEPARTMENT_LIST;
					priorDiploma.setValue(NPMBFormConstants.PRIOR_DIPLOMA_NAME_LIST[1]);
					priorDiploma.setEnabled(true);
					clearAndEnable(true, hndFields);
					clearAndEnable(false, ndFields);
					educationValidators.clear();
			    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_EDUCATION_HND_VALIDATORS, educationValidators);					
				}
				FormUtils.setupListBox(departmentType, options);
			}
		});
		priorDiploma.setEnabled(false);
		priorDiploma.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if(event.getValue().equals(NPMBFormConstants.PRIOR_DIPLOMA_NAME_LIST[0]))
					diplomaType.setValue(DTOConstants.ND, true);
			}
		});

		
		ValueChangeHandler<String> declarationHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				setDeclarationAuthor(getFormData());
				
			}
		};
		surname.addValueChangeHandler(declarationHandler);
		firstName.addValueChangeHandler(declarationHandler);
		middleName.addValueChangeHandler(declarationHandler);
		declarationContent.setText(NPMBFormConstants.DECLARATION);
		BSDatePicker dobBox = (BSDatePicker) dob.getFieldWidget();
		dobBox.setStartView(DateTimePickerView.DECADE);
		dobBox.setMinView(DateTimePickerView.MONTH);
		dobBox.setFormat("yyyy M d");
		dobBox.setValue(null);
		Date startDate = new Date();
		CalendarUtil.addDaysToDate(startDate, -(365 * 40));
		CalendarUtil.setToFirstDayOfMonth(startDate);
		CalendarUtil.resetTime(startDate);
		dobBox.setStartDate(startDate);
		dobBox.setAutoClose(true);
		dobBox.reload();

		//setup click handlers
		ClickHandler saveHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				submitHandler((Button) event.getSource());
			}
		};
		
		ClickHandler submitHandler = new ClickHandler() {
			
			@Override
			public void onClick(final ClickEvent event) {
				final Button clickSource = (Button) event.getSource();
				String msg = "Click ok to confirm you have read and agree with the terms stated on this page. " +
						"Please note that you will not be able to edit your application details after this step";
				Bootbox.confirm(msg, new ConfirmCallback() {
					
					@Override
					public void callback(boolean result) {
						if(result)
							submitHandler(clickSource);
					}
				});
				
			}
		};
		
		setupValidators();
		personalSave.addClickHandler(saveHandler);
		familySave.addClickHandler(saveHandler);
		educationSave.addClickHandler(saveHandler);
		submit.addClickHandler(submitHandler);
	}
	
	private void clearAndEnable(boolean isEnabled, BSTextWidget<String>[] fields)
	{
		for(BSTextWidget<String> f : fields)
		{
			f.clear();
			f.setEnabled(isEnabled);
		}
		
	}
	
	public void enableTabs(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setEnabled(true);
		case APPLICATION_FAMILY:
			familyHeader.setEnabled(true);
		case APPLICATION_EDUCATION:
			educationHeader.setEnabled(true);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setEnabled(true);
		}
	}
	
	public void activatePanel(WorkflowStateInstance state)
	{
		deactivatePanels();
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setActive(true);
			signatureBody.setActive(true);
			break;
		case APPLICATION_FAMILY:
			familyHeader.setActive(true);
			familyBody.setActive(true);
			break;
		case APPLICATION_EDUCATION:
			educationHeader.setActive(true);
			educationBody.setActive(true);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setActive(true);
			personalBody.setActive(true);
		}		
	}
	
	public void deactivatePanels()
	{
		signatureHeader.setActive(false);
		signatureBody.setActive(false);
		familyHeader.setActive(false);
		familyBody.setActive(false);
		educationHeader.setActive(false);
		educationBody.setActive(false);
		personalHeader.setActive(false);
		personalBody.setActive(false);
	}

	public void priorPanel(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_CONFIRM:
			switchPanel(signatureHeader, familyHeader, signatureBody, familyBody);
			break;
		case APPLICATION_FAMILY:
			switchPanel(familyHeader, educationHeader, familyBody, educationBody);
			break;
		case APPLICATION_EDUCATION:
			switchPanel(educationHeader, personalHeader, educationBody, personalBody);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}
	
	public void nextPanel(WorkflowStateInstance state)
	{
		boolean allowJump = !ClientUtils.NPMBUserCookie.getCookie().isOps();
		switch (state) {
		case APPLICATION_CONFIRM:
			if(allowJump) History.newItem(NameTokens.PRINT + "/" + loanId);
			break;
		case APPLICATION_FAMILY:
			switchPanelToNext(familyHeader, signatureHeader, familyBody, signatureBody);
			break;
		case APPLICATION_EDUCATION:
			switchPanelToNext(educationHeader, familyHeader, educationBody, familyBody);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			switchPanelToNext(personalHeader, educationHeader, personalBody, educationBody);
			break;
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:			
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}

	private void switchPanel(TabListItem currentHeader, TabListItem switchedHeader, TabPane currentBody, TabPane switchedBody)
	{
		currentHeader.setActive(false);
		currentBody.setActive(false);
		switchedHeader.setActive(true);
		switchedBody.setActive(true);
	}
	
	
	private void switchPanelToNext(TabListItem currentHeader, TabListItem nextHeader, TabPane currentBody, TabPane nextBody)
	{
		if(!currentHeader.isActive()) //something else is active, and yes this is a hack
			deactivatePanels();
		switchPanel(currentHeader, nextHeader, currentBody, nextBody);
		nextHeader.setEnabled(true);
	}
	
	public void submitHandler(Button b) 
	{
		HashMap<String, String> customerInfo = getFormData();
		customerInfo.put(NPMBFormConstants.ID_STATE, buttonToStateMap.get(b).toString());
		if(validateFields(b))
			submitFormData(customerInfo, b);
	}
	
	public void enableButtons(boolean enabled)
	{
		personalSave.setEnabled(enabled);
		educationSave.setEnabled(enabled);
		familySave.setEnabled(enabled);
		submit.setEnabled(enabled);
	}
	
	private void submitFormData(HashMap<String, String> data, Button b)
	{
		GWT.log("Loan ID: " + loanId);
		ArrayList<HashMap<String, String>>[] suppData = getFormTables();
		enableButtons(false); //prevent user from hitting save multiple times while waiting on server
		if(b == submit)
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, true, submitCallback);
		else
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, false, saveCallback);
	}	
	
	private boolean validateFields(Button button)
	{
		errorMap.clear();
		TabContent x;
		
		ArrayList<BSControlValidator> validators;
		if(button == personalSave)
			validators = personalValidators;
		else if(button == educationSave)
		{
			validators = educationValidators;
			if(olevOneSlot.getAllRows().size() < 5)
				errorMap.put("1st Sitting O Levels", "Please select total number of subjects taken for your first sitting o-level" +
						" and then enter each subject name and your grade/result");
			if(!NPMBFormConstants.O_LEV_NAME_LIST2[0].equals(olevTwoName.getValue()))
			{
				if(olevOneSlot.getAllRows().size() < 5)
					errorMap.put("2nd Sitting O Levels", "Please select total number of subjects taken for your second sitting o-level" +
							" and then enter each subject name and your grade/result");
			}
		}
		else if(button == familySave)
		{
			validators = familyValidators;
			if(guardianSlot.getAllRows().size() == 0)
				errorMap.put("No of Parents and/or Guardians", "Please select at least one parent/guardian and then enter their details");
		}
		else
		{
			validators = new ArrayList<BSControlValidator>();
			validators.addAll(personalValidators);
			validators.addAll(educationValidators);
			validators.addAll(familyValidators);
		}
		
		for(BSControlValidator v : validators)
			v.markErrors();		
	
		if(errorMap.size() > 0)
		{
			StringBuilder b = new StringBuilder(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found.<ul>");
			for(String val : errorMap.values())
				b.append("<li>").append(val).append("</li>");
			b.append("</ul>");
			CustomerAppHelper.showErrorMessage(b.toString());
		}
		return errorMap.size() == 0;
	}	
	
	ArrayList<BSControlValidator> personalValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> educationValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> familyValidators = new ArrayList<BSControlValidator>();
	HashMap<String, String> errorMap = new HashMap<String, String>();

	HashMap<String, BSTextWidget> controlWidgetsMap = new HashMap<String, BSTextWidget>();
	private void setupValidators()
	{
    	controlWidgetsMap.put(NPMBFormConstants.EMAIL, email);
    	controlWidgetsMap.put(NPMBFormConstants.TEL_NO, phone);
    	controlWidgetsMap.put(NPMBFormConstants.SURNAME, surname);
    	controlWidgetsMap.put(NPMBFormConstants.FIRST_NAME, firstName);
    	controlWidgetsMap.put(NPMBFormConstants.RESIDENTIAL_ADDRESS, residentialAddress);
    	controlWidgetsMap.put(NPMBFormConstants.DATE_OF_BIRTH, dob);
    	controlWidgetsMap.put(NPMBFormConstants.MOBILE_NO, alternateNo);
    	
    	
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_EMAIL, spouseEmail);
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_PHONE, spousePhone);
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_NAME, spouseName);
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_ADDRESS, spouseAddress);
    	
    	controlWidgetsMap.put(NPMBFormConstants.IT_PLACEMENT, itName);
    	controlWidgetsMap.put(NPMBFormConstants.IT_ADDRESS, itAddress);
    	controlWidgetsMap.put(NPMBFormConstants.IT_START, itStart);
    	controlWidgetsMap.put(NPMBFormConstants.IT_END, itEnd);
    	
    	
    	controlWidgetsMap.put(NPMBFormConstants.PRIOR_GPA, priorGPA);
    	controlWidgetsMap.put(NPMBFormConstants.PRIOR_GPA_MAX, priorMaxGPA);
    	controlWidgetsMap.put(NPMBFormConstants.PRIOR_DEGREE_SCHOOL, priorSchool);
    	controlWidgetsMap.put(NPMBFormConstants.PRIOR_YEAR, priorYear);
    	controlWidgetsMap.put(NPMBFormConstants.O_LEV_ONE_CREDITS, olevOneCredits);
    	controlWidgetsMap.put(NPMBFormConstants.O_LEV_TWO_CREDITS, olevTwoCredits);
    	 	
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_PERSONAL_VALIDATORS, personalValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_EDUCATION_VALIDATORS, educationValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_FAMILY_VALIDATORS, familyValidators);
	}
	
	private void setupValidator(HashMap<String, BSTextWidget> controlWidgetsMap, HashMap<String, FormValidator[]> validationSet, ArrayList<BSControlValidator> targetValidatorList)
	{
		for(String formKey : validationSet.keySet())
		{
			FormValidator[] fieldValidator = validationSet.get(formKey);
			BSTextWidget inputField = controlWidgetsMap.get(formKey);
			for(FormValidator v : fieldValidator)
				if(v.equals(FormValidator.MANDATORY))
					inputField.markRequired();
			BSLoanFieldValidator submitValidator = new BSLoanFieldValidator(fieldValidator,inputField, formKey, errorMap, true);
			targetValidatorList.add(submitValidator);
		}
	}
	
	public ArrayList<HashMap<String, String>>[] getFormTables()
	{
		ArrayList<HashMap<String, String>>[] result = new ArrayList[3];
		result[NPMBFormConstants.PARENT_IDX] = guardianSlot.getAllRows();
		result[NPMBFormConstants.OLEVEL_ONE_IDX] = olevOneSlot.getAllRows();
		result[NPMBFormConstants.OLEVEL_TWO_IDX] = olevTwoSlot.getAllRows();
		return result;
	}
	
	public void setFormTables(ArrayList<HashMap<String, String>>[] tables)
	{		
		if(tables == null)
		{
			GWT.log("Null Table");
			return;
		}
		GWT.log("Not Null Table");
		guardianSlot.initRows(tables[NPMBFormConstants.PARENT_IDX]);
		olevOneSlot.initRows(tables[NPMBFormConstants.OLEVEL_ONE_IDX]);
		olevTwoSlot.initRows(tables[NPMBFormConstants.OLEVEL_TWO_IDX]);
	}
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(NPMBFormConstants.SURNAME, surname.getValue());
		result.put(NPMBFormConstants.FIRST_NAME, firstName.getValue());
		result.put(NPMBFormConstants.MIDDLE_NAME, middleName.getValue());
		result.put(NPMBFormConstants.RESIDENTIAL_ADDRESS, residentialAddress.getValue());
		result.put(NPMBFormConstants.SPOUSE_ADDRESS, spouseAddress.getValue());
		result.put(NPMBFormConstants.ADDRESS, mailingAddress.getValue());
		result.put(NPMBFormConstants.TEL_NO, phone.getValue());		
		result.put(NPMBFormConstants.MOBILE_NO, alternateNo.getValue());
		result.put(NPMBFormConstants.EMAIL, email.getValue());
		result.put(NPMBFormConstants.DATE_OF_BIRTH, dob.getDisplayText());
		result.put(NPMBFormConstants.PLACE_OF_BIRTH, pob.getValue());
		result.put(NPMBFormConstants.GENDER, gender.getValue());		
		result.put(NPMBFormConstants.NATIONALITY, nationality.getValue());
		result.put(NPMBFormConstants.ORIGIN_STATE, origin.getValue());
		result.put(NPMBFormConstants.MARITAL_STATUS, maritalStatus.getValue());
		result.put(NPMBFormConstants.SPOUSE_NAME, spouseName.getValue());				
		result.put(NPMBFormConstants.SPOUSE_EMAIL, spouseEmail.getValue());
		result.put(NPMBFormConstants.SPOUSE_PHONE, spousePhone.getValue());
		result.put(NPMBFormConstants.LGA, lga.getValue());

		result.put(NPMBFormConstants.DIPLOMA, diplomaType.getValue());
		result.put(NPMBFormConstants.ADMISSION_TYPE, sessType.getValue());
		result.put(NPMBFormConstants.DEPARTMENT, departmentType.getValue());
		result.put(NPMBFormConstants.JAMB_NO, jambNo.getValue());
		result.put(NPMBFormConstants.JAMB_SCORE, jambScore.getValue());
		result.put(NPMBFormConstants.JAMB_YEAR, jambYear.getValue());
		result.put(NPMBFormConstants.O_LEV_ONE_NAME, olevOneName.getValue());		
		result.put(NPMBFormConstants.O_LEV_ONE_YEAR, olevOneYear.getValue());
		result.put(NPMBFormConstants.O_LEV_ONE_CREDITS, olevOneCredits.getValue());
		result.put(NPMBFormConstants.O_LEV_TWO_NAME, olevTwoName.getValue());		
		result.put(NPMBFormConstants.O_LEV_TWO_YEAR, olevTwoYear.getValue());
		result.put(NPMBFormConstants.O_LEV_TWO_CREDITS, olevTwoCredits.getValue());		
		result.put(NPMBFormConstants.PRIOR_DEGREE, priorDiploma.getValue());
		result.put(NPMBFormConstants.PRIOR_DEGREE_SCHOOL, priorSchool.getValue());
		result.put(NPMBFormConstants.PRIOR_YEAR, priorYear.getValue());		
		result.put(NPMBFormConstants.PRIOR_GPA, priorGPA.getValue());
		result.put(NPMBFormConstants.PRIOR_GPA_MAX, priorMaxGPA.getValue());
		result.put(NPMBFormConstants.PRIOR_SESS_TYPE, priorSessType.getValue());

		result.put(NPMBFormConstants.IT_ADDRESS, itAddress.getValue());
		result.put(NPMBFormConstants.IT_START, itStart.getValue());
		result.put(NPMBFormConstants.IT_END, itEnd.getValue());		
		result.put(NPMBFormConstants.IT_PHONE, itPhone.getValue());
		result.put(NPMBFormConstants.IT_PLACEMENT, itName.getValue());
		
		return result;
	}
	
	public void setFormData(HashMap<String, String> result)
	{
		surname.setValue(result.get(NPMBFormConstants.SURNAME));
		firstName.setValue(result.get(NPMBFormConstants.FIRST_NAME));
		middleName.setValue(result.get(NPMBFormConstants.MIDDLE_NAME));
		residentialAddress.setValue(result.get(NPMBFormConstants.RESIDENTIAL_ADDRESS));
		spouseAddress.setValue(result.get(NPMBFormConstants.SPOUSE_ADDRESS));
		mailingAddress.setValue(result.get(NPMBFormConstants.ADDRESS));
		phone.setValue(result.get(NPMBFormConstants.TEL_NO));
		alternateNo.setValue(result.get(NPMBFormConstants.MOBILE_NO));
		email.setValue(result.get(NPMBFormConstants.EMAIL));
		dob.setDisplayText(result.get(NPMBFormConstants.DATE_OF_BIRTH));
		pob.setValue(result.get(NPMBFormConstants.PLACE_OF_BIRTH));
		gender.setValue(result.get(NPMBFormConstants.GENDER));		
		email.setValue(result.get(NPMBFormConstants.EMAIL));
		nationality.setValue(result.get(NPMBFormConstants.NATIONALITY));
		origin.setValue(result.get(NPMBFormConstants.ORIGIN_STATE));
		lga.setValue(result.get(NPMBFormConstants.LGA));

		spouseName.setValue(result.get(NPMBFormConstants.SPOUSE_NAME));
		spouseEmail.setValue(result.get(NPMBFormConstants.SPOUSE_EMAIL));
		spousePhone.setValue(result.get(NPMBFormConstants.SPOUSE_PHONE));
		maritalStatus.setValue(result.get(NPMBFormConstants.MARITAL_STATUS));
		
		diplomaType.setValue(result.get(NPMBFormConstants.DIPLOMA));
		sessType.setValue(result.get(NPMBFormConstants.ADMISSION_TYPE));
		departmentType.setValue(result.get(NPMBFormConstants.DEPARTMENT));
		jambNo.setValue(result.get(NPMBFormConstants.JAMB_NO));
		jambScore.setValue(result.get(NPMBFormConstants.JAMB_SCORE));
		jambYear.setValue(result.get(NPMBFormConstants.JAMB_YEAR));
		olevOneName.setValue(result.get(NPMBFormConstants.O_LEV_ONE_NAME));		
		olevOneYear.setValue(result.get(NPMBFormConstants.O_LEV_ONE_YEAR));
		olevOneCredits.setValue(result.get(NPMBFormConstants.O_LEV_ONE_CREDITS));
		olevTwoName.setValue(result.get(NPMBFormConstants.O_LEV_TWO_NAME));		
		olevTwoYear.setValue(result.get(NPMBFormConstants.O_LEV_TWO_YEAR));
		olevTwoCredits.setValue(result.get(NPMBFormConstants.O_LEV_TWO_CREDITS));		
		priorDiploma.setValue(result.get(NPMBFormConstants.PRIOR_DEGREE));
		priorSchool.setValue(result.get(NPMBFormConstants.PRIOR_DEGREE_SCHOOL));
		priorYear.setValue(result.get(NPMBFormConstants.PRIOR_YEAR));		
		priorGPA.setValue(result.get(NPMBFormConstants.PRIOR_GPA));
		priorMaxGPA.setValue(result.get(NPMBFormConstants.PRIOR_GPA_MAX));
		
		priorSessType.setValue(result.get(NPMBFormConstants.PRIOR_SESS_TYPE));		
		itStart.setValue(result.get(NPMBFormConstants.IT_START));
		itEnd.setValue(result.get(NPMBFormConstants.IT_END));
		itName.setValue(result.get(NPMBFormConstants.IT_PLACEMENT));		
		itAddress.setValue(result.get(NPMBFormConstants.IT_ADDRESS));
		itPhone.setValue(result.get(NPMBFormConstants.IT_PHONE));
		
		
		setDeclarationAuthor(result);
	}

	public void enableEditing(boolean isEdit)
	{
		boolean enabled = isEdit;
		surname.setEnabled(enabled);
		firstName.setEnabled(enabled);
		middleName.setEnabled(enabled);
		residentialAddress.setEnabled(enabled);
		spouseAddress.setEnabled(enabled);
		mailingAddress.setEnabled(enabled);
		phone.setEnabled(enabled);		
		email.setEnabled(false);
		dob.setEnabled(enabled);
		pob.setEnabled(enabled);
		gender.setEnabled(enabled);
		nationality.setEnabled(enabled);
		origin.setEnabled(enabled);
		lga.setEnabled(enabled);

		spouseName.setEnabled(enabled);				
		spouseEmail.setEnabled(enabled);
		spousePhone.setEnabled(enabled);

		guardianSlot.setEnabled(enabled);
		olevOneSlot.setEnabled(enabled);
		olevTwoSlot.setEnabled(enabled);

		maritalStatus.setEnabled(enabled);
		alternateNo.setEnabled(enabled);
		
		diplomaType.setEnabled(enabled);
		sessType.setEnabled(enabled);
		departmentType.setEnabled(enabled);
		jambNo.setEnabled(enabled);
		jambScore.setEnabled(enabled);
		jambYear.setEnabled(enabled);
		olevOneName.setEnabled(enabled);		
		olevOneYear.setEnabled(enabled);
		olevOneCredits.setEnabled(enabled);
		olevTwoName.setEnabled(enabled);		
		olevTwoYear.setEnabled(enabled);
		olevTwoCredits.setEnabled(enabled);		
		priorDiploma.setEnabled(enabled);
		priorSchool.setEnabled(enabled);
		priorYear.setEnabled(enabled);		
		priorGPA.setEnabled(enabled);
		priorMaxGPA.setEnabled(enabled);
		priorSessType.setEnabled(enabled);
		itAddress.setEnabled(enabled);
		itPhone.setEnabled(enabled);
		itName.setEnabled(enabled);
		itStart.setEnabled(enabled);
		itEnd.setEnabled(enabled);
		
		enableButtons(enabled);
	}	
	
	public void setDeclarationAuthor(HashMap<String, String> result)
	{
		String[] nameParts = {result.get(NPMBFormConstants.SURNAME), result.get(NPMBFormConstants.FIRST_NAME),
				result.get(NPMBFormConstants.MIDDLE_NAME)};
		declarationAuthor.setText(FormUtils.getFullName(nameParts));		
	}
	
	public void clear()
	{
		setFormData(new HashMap<String, String>());
		guardianSlot.clear();
		olevOneSlot.clear();
		olevTwoSlot.clear();
		enableButtons(false);
	}
	
	public void setLoanID(String loanID) 
	{
		clear();
		CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(loanID, fetchFormDataCallback);
	}
}
