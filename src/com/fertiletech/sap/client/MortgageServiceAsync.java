package com.fertiletech.sap.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface MortgageServiceAsync {

	void saveLoanApplicationData(HashMap<String, String> appData,
			ArrayList<HashMap<String, String>>[] supplementaryData,
			String loanKeyStr, boolean isSubmit,
			AsyncCallback<String[]> callback);

	void startLoanApplication(HashMap<String, String> appData,
			AsyncCallback<String[]> callback);

	void getStoredLoanApplication(String loanKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getLoanState(String loanID, AsyncCallback<TableMessage> callback);

	void getLoanState(long loanID, AsyncCallback<TableMessage> callback);

	void getLoanApplicationWithLoanID(String loanKeyStr,
			AsyncCallback<HashMap<String, String>> callback);

	void getSupplementaryData(String loanKeyStr,
			AsyncCallback<ArrayList<HashMap<String, String>>[]> callback);

	void fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header, AsyncCallback<String> callback);

	void getSaleLeads(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	void getApplicationFormDownloadLink(String appFormID,
			AsyncCallback<String[]> appDownloadLinkCallback);

	void getSalesLeadCollection(String loanID,
			AsyncCallback<List<TableMessage>> callback);

	void changeApplicationState(String loanStr, WorkflowStateInstance state,
			String message, AsyncCallback<String> callback);

	void getLeadAggregates(Date startDate, Date endDate,
			AsyncCallback<HashMap<WorkflowStateInstance, Integer>> callback);
}
