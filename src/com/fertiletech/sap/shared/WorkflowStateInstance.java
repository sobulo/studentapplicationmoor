package com.fertiletech.sap.shared;

import java.util.HashSet;

public enum WorkflowStateInstance {
	
	APPLICATION_STARTED, APPLICATION_PERSONAL, 
	APPLICATION_EDUCATION, APPLICATION_FAMILY, 
	APPLICATION_CONFIRM, APPLICATION_PENDING,
	APPLICATION_CANCEL, APPLICATION_DENIED, 
	APPLICATION_APPROVED;
	
	private static HashSet<WorkflowStateInstance> APPROVED_STATES = new HashSet<WorkflowStateInstance>();
	private static HashSet<WorkflowStateInstance> DENIED_STATES = new HashSet<WorkflowStateInstance>();
	
	static
	{
		APPROVED_STATES.add(APPLICATION_APPROVED);
		DENIED_STATES.add(APPLICATION_DENIED);
	}
	
	public Boolean isApprovedState()
	{
		if(APPROVED_STATES.contains(this))
			return true;
		else if(DENIED_STATES.contains(this))
			return false;
		else
			return null;
	}

	public String getDisplayString()
	{
		switch (this) {
		case APPLICATION_APPROVED:
			return "Admission Approved";
		case APPLICATION_CANCEL:
			return "Canceled";
		case APPLICATION_CONFIRM:
			return "Signature Page";
		case APPLICATION_DENIED:
			return "Admission Denied";
		case APPLICATION_EDUCATION:
			return "Education Page";
		case APPLICATION_FAMILY:
			return "Family Page";
		case APPLICATION_PENDING:
			return "Under FCAHPT Review";
		case APPLICATION_PERSONAL:
			return "Personal Page";
		case APPLICATION_STARTED:
			return "Start Page";
		}
		return "Error - Contact IT";
	}
	
	public String getChartColor()
	{
		switch (this) {
		case APPLICATION_APPROVED:
			return "color: green";
		case APPLICATION_CANCEL:
			return "color: grey";
		case APPLICATION_CONFIRM:
			return "color: #e072df";
		case APPLICATION_DENIED:
			return "color: BLACK";
		case APPLICATION_EDUCATION:
			return "color: brown";
		case APPLICATION_FAMILY:
			return "color: blue";
		case APPLICATION_PENDING:
			return "color: purple";
		case APPLICATION_PERSONAL:
			return "color: orange";
		case APPLICATION_STARTED:
			return "color: yellow";
		}
		return "color: magenta";
	}
	
}
