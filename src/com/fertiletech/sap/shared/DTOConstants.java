/**
 * 
 */
package com.fertiletech.sap.shared;


/**
 * @author Segun Razaq Sobulo
 *
 */
public final class DTOConstants {
	public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";	
	public final static String GCS_LOAN_ID_PARAM = "com.fertiletech.gcsloan";
    public final static int TM_DIFF_HEADER = 0;
    public final static int TM_DIFF_VALS = 1;
    public final static int TM_DIFF_POS = 2;
    
    public final static String APP_PARAM_ADMINS = "user-admin";
    public final static String APP_PARAM_EDITOR = "user-editor";
    public final static String APP_PARAM_REVIEWER = "user-reviewer";
    public final static String APP_PARAM_MAP_TITLES = "map-titles";
    
	public final static int CATEGORY_IDX = 0;
	public final static int DETAIL_IDX = 1;
	public final static int LAT_IDX = 0;
	public final static int LNG_IDX = 1;    
    
    //loan mkt param strings
    public final static String MAX_PRINCIPAL_KEY = "XP";
    public final static String MIN_PRINCIPAL_KEY = "NP";
    public final static String MAX_TENOR_KEY = "XT";
    public final static String MIN_TENOR_KEY = "NT";
    public final static String INTEREST_KEY = "I";
    
    public final static int READY_IDX = 10;
    public final static int HOT_IDX = 0;
    public final static int COLD_IDX = 1;
    public final static int VERIFY_IDX = 2;
    public final static int APPROVE_IDX = 3;
    public final static int CLOSED_IDX = 4;
    public final static int ALL_IDX = 5; //this idx value should be higher than all others
    
    public final static int LOAN_DENIED_PICTURE_STATE = 2;
    
    public final static String[] OPS_SUGGESTIONS = {"Confirm as a prospect?", "Deny immediately?", "All documents received?", "Approve loan?", "Enter Orbit ID?"};


    //TODO options for setting up admission calendar - move to config pref object
    public final static int CURRENT_ADMISSION_YEAR = 2015;
    public final static int[] ADMISSION_OPTIONS = {2015};
    
    public final static String PT = "Part Time";
    public final static String FT = "Full Time";
    public final static String ND = "National Diploma";
    public final static String HND = "Higher National Diploma";
    
    //fields for displaying sales leads to users
    
    public final static int PUB_ADM_DIPLOMA = 0;
    public final static int PUB_ADM_DEPARTMENT = 1;
    public final static int PUB_ADM_YEAR = 2;
    public final static int PUB_ADM_FULL_TIME = 3;
    public final static int PUB_STATUS_IDX = 4;
    public final static int PUB_MSG_IDX = 5;    
    public final static int PUB_CRT_IDX = 0;
    public final static int PUB_MOD_IDX = 1;
     
	//text indices
	public final static int FULL_NAME_IDX = 0;
	public final static int EMAIL_IDX = 1;
	public final static int PHONE_NUMBER_IDX = 2;
	public final static int ADM_FULL_TIME_IDX = 3;
	public final static int ADM_ND_IDX = 4;
	public final static int ADM_DEPT_IDX = 5;
	public final static int NOTE_IDX = 6;
	public final static int DIPL_SCHOOL_NAME = 7;
	
	
	//number indices
    public final static int LEAD_ID_IDX = 0;
	public final static int OLEV1_IDX = 1;
	public final static int OLEV2_IDX = 2;
	public final static int DIPL_GPA_IDX = 1;
	public final static int DIPL_MAX_GPA_IDX = 2;
    
    //date indices
    public final static int DATE_CREATED_IDX = 0;
    public final static int DATE_MODIFIED_IDX = 1;
    //------------------------------------------------ 
    
    //ID indices
    public final static int LOAN_KEY_IDX = 0;
    public final static int LOAN_STATUS_IDX = 1;
    public final static int LOAN_MESSAGE_IDX = 2;
    
    //below should be deprecated
    public final static int FORM_KEY_IDX = 1;
    public final static int SUBMITTED_KEY_IDX = 2; //only the form not ack objects etc 
    
    public final static int LOAN_IDX = 0;
    public final static int LOAN_ID_IDX = 1;
    public final static int LOAN_STATE_IDX = 2;
    public final static int LOAN_EDIT_IDX = 3;
	public static final String NPMB_FORM_HEADER = "npmb-header-pdf";
	public static final String NPMB_FORM_LOGO = "npmb-logo-pdf";
	public static final String PDF_NO_IMAGE = "passportbox";

}
