package com.fertiletech.sap.shared;

import java.util.Arrays;
import java.util.HashMap;

import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;


public class NPMBFormConstants {
	
	//personal fields
	public static String ID = "FCAHPT-Portal-ID";
	public static String ID_NUM = "FCAHPT-Portal-NO";
	public static String ID_STATE = "FCAHPT-State";
	public static String ID_EDIT = "FCAHPT-Write";
	public static String SURNAME = "Surname"; //k
	public static String FIRST_NAME = "First Name"; //k
	public static String MIDDLE_NAME = "Middle Name"; //k
	public static String ADDRESS = "Address";  //k
	public static String TEL_NO = "Tel No";  //k
	public static String MOBILE_NO = "Alternate No"; //k
	public static String EMAIL = "Personal Email";
	public static String DATE_OF_BIRTH = "Date of Birth"; //k
	public static String PLACE_OF_BIRTH = "Place of Birth"; //k
	public static String GENDER = "Sex"; //k
	public static String ORIGIN_STATE = "State of Origin"; //k
	public static String LGA = "L.G.A";
	public static String NATIONALITY = "Nationality";	//k
	public static String RESIDENTIAL_ADDRESS = "Residential Address"; //k
	
	//family
	public static String MARITAL_STATUS = "Marital Status";
	public static String SPOUSE_NAME = "Next of Kin";
	public static String SPOUSE_ADDRESS = "Kin Address";
	public static String SPOUSE_EMAIL = "Kin Email";
	public static String SPOUSE_PHONE = "Kin Phone";	
	
	//public static String NO_OF_CHILDREN = "Children/Other Dependents";
	//public static String NO_OF_PARENTS = "Parents/Guardians"; //needs to be added
	
	//admission type
	public static String DIPLOMA = "ND/HND";
	public static String ADMISSION_TYPE = "Student Type";
	public static String DEPARTMENT = "Department";
	
	public static String JAMB_NO = "Jamb Reg No";
	public static String JAMB_SCORE = "Jamb Score";
	public static String JAMB_YEAR = "Jamb Year";
	
	public static String O_LEV_ONE_NAME = "O-Level 1st Sitting";
	public static String O_LEV_ONE_YEAR = "Year of 1st Sitting";
	public static String O_LEV_ONE_CREDITS = "No. of Credits - 1st Sitting";
	
	public static String O_LEV_TWO_NAME = "O-Level 2nd Sitting";
	public static String O_LEV_TWO_YEAR = "Year of 2nd Sitting";
	public static String O_LEV_TWO_CREDITS = "No. of Credits - 2nd Sitting";

	
	public static String PRIOR_DEGREE = "ND/NCE";
	public static String PRIOR_DEGREE_SCHOOL = "Polytechnic/College Attended";
	public static String PRIOR_YEAR = "Graduation Year";
	
	public static String PRIOR_GPA = "Graduation G.P.A";		
	public static String PRIOR_GPA_MAX = "Maximum Possible G.P.A";
	public static String PRIOR_SESS_TYPE = "Prior Type";
	
	public static String IT_PLACEMENT = "I.T. Placement/Employer";
	public static String IT_ADDRESS = "I.T. Employer Address";
	public static String IT_PHONE = "I.T. Employer Tel. No";
	public static String IT_START = "I.T. Start Month/Year";
	public static String IT_END = "I.T. End Month/Year";
	
		
	
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "I hereby declare that all information provided in this application are correct and" +
			" that all copies of documents I will submit are authentic. I understand that the Federal College of Animal Health and Production Technology will verify the information supplied. " +
			"I therefore agree that any material misstatement/falsification of information discovered renders my application null and void.";
	
	public static int PARENT_IDX = 0;
	public static int OLEVEL_ONE_IDX = 1;
	public static int OLEVEL_TWO_IDX = 2;
	
	public static String[] TABLE_DESCRIPTIONS = {"Parents/Guardians", "O-Level 1st Sitting Subjects", "O-Level 2nd Sitting Subjects"};
	
	
	public final static String[] GENDER_LIST = {"Male", "Female"};
	public final static String[] MARITAL_STATUS_LIST = {"Married", "Single", "Divorced/Separated", "Widowed"};
	public final static String[] O_LEV_NAME_LIST = {"WAEC", "GCE", "NECO", "NABTEB", "Other"};
	public final static String[] O_LEV_NAME_LIST2 = {"Not Applicable", "WAEC", "GCE", "NECO", "NABTEB", "Other"};
	public final static String[] DIPLOMA_NAME_LIST = {DTOConstants.ND, DTOConstants.HND};
	public final static String[] PRIOR_DIPLOMA_NAME_LIST = {"Not Applicable", "National Diploma", "National Certificate of Education"};
	public final static String[] ND_DEPARTMENT_LIST = {"Animal Health & Production Technology", "Fisheries", 
														"Science Laboratory Technology", "Statistics", "Computer Science"};
	public final static String[] HND_DEPARTMENT_LIST = {"Agricultural Extension & Management", "Animal Production Technology", "Animal Health Technology"};
	public final static String[] ADM_TYPE_LIST = {DTOConstants.FT, DTOConstants.PT};
	
	//flex panel headers and widths
	public static String[][] PARENT_TABLE_CFG = {{"Name", "Relationship", "Profession", "Phone"},{"35", "20", "25", "20"}, {"parent or guardian", "parents or guardians"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] OLEV_TABLE_CFG = {{"Subject", "Result"},{"50", "50"}, {"subject", "subjects"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] OLEV_TABLE2_CFG = {{"Subject", "Result"},{"50", "50"}, {"subject", "subjects"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][][] ALL_CONFIG = {PARENT_TABLE_CFG, OLEV_TABLE_CFG, OLEV_TABLE2_CFG};
	
	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_FAMILY_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EDUCATION_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EDUCATION_HND_VALIDATORS = new HashMap<String, FormValidator[]>();


	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] mandatoryPlusDateParsesOk = {FormValidator.MANDATORY, FormValidator.DATE_OLD_ENOUGH};
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};		
    	FormValidator[] atLeastOneParsesOk = {FormValidator.PARSES_INTEGER_1};		
    	FormValidator[] atLeastFiveParsesOk = {FormValidator.PARSES_INTEGER_5};		

    	
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(RESIDENTIAL_ADDRESS, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(DATE_OF_BIRTH, mandatoryPlusDateParsesOk);
    	NPMB_PERSONAL_VALIDATORS.put(MOBILE_NO, phoneOnly);
    	

    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_EMAIL, emailOnly);
    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_PHONE, phoneOnly);
    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_NAME, mandatoryOnly);
    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_ADDRESS, mandatoryOnly);
    	
    	NPMB_EDUCATION_HND_VALIDATORS.put(IT_ADDRESS, mandatoryOnly);
    	NPMB_EDUCATION_HND_VALIDATORS.put(IT_PLACEMENT, mandatoryOnly);
    	NPMB_EDUCATION_HND_VALIDATORS.put(IT_START, mandatoryOnly);
    	NPMB_EDUCATION_HND_VALIDATORS.put(IT_END, mandatoryOnly);

    	NPMB_EDUCATION_HND_VALIDATORS.put(PRIOR_GPA, mandatoryPlusLoanParsesOk);
    	NPMB_EDUCATION_HND_VALIDATORS.put(PRIOR_GPA_MAX, mandatoryPlusLoanParsesOk);
    	NPMB_EDUCATION_HND_VALIDATORS.put(PRIOR_DEGREE_SCHOOL, mandatoryOnly);
    	NPMB_EDUCATION_HND_VALIDATORS.put(PRIOR_YEAR, mandatoryPlusParsesOk);
    	NPMB_EDUCATION_HND_VALIDATORS.put(O_LEV_ONE_CREDITS, mandatoryPlusParsesOk);
    	NPMB_EDUCATION_HND_VALIDATORS.put(O_LEV_TWO_CREDITS, atLeastOneParsesOk);
    	
    	NPMB_EDUCATION_VALIDATORS.put(O_LEV_ONE_CREDITS, mandatoryPlusParsesOk);
    	NPMB_EDUCATION_VALIDATORS.put(O_LEV_TWO_CREDITS, atLeastOneParsesOk);


	}
	
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		
		long subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[1];
		subs[0] = appSubmitted;
		return subs;
	}	
	

}
