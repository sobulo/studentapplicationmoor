/**
 * 
 */
package com.fertiletech.sap.shared;

import java.io.Serializable;

/**
 * @author Segun Razaq Sobulo
 *
 */
public enum LoginRoles implements Serializable{
	    ROLE_SUPER_ADMIN, ROLE_BANK_STAFF, ROLE_PUBLIC ;	
}
