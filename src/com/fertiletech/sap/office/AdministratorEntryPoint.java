package com.fertiletech.sap.office;

import com.fertiletech.sap.server.login.oauth.OurOAuthParams;
import com.floreysoft.gwt.picker.client.utils.PickerLoader;
import com.google.gwt.core.client.EntryPoint;

public class AdministratorEntryPoint implements EntryPoint {
	private String API_KEY = "AIzaSyAkTkJOCN0RgnBAuU7ankZvmmsf4jjDHtQ";

	@Override
	public void onModuleLoad() {
	    PickerLoader.loadApi(OurOAuthParams.GOOGLE_API_SECRET, new Runnable() {
	        public void run() {
	        	new Showcase();
	        }
	      });		
	}
}
