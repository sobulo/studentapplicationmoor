package com.fertiletech.sap.office.admin;

import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class MapTypesCW extends CWEditParamsBase{

	public MapTypesCW() {
		super("Map Types", "Edit map titles available for selection when attaching maps on the application status page");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_MAP_TITLES;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(MapTypesCW.class, super.getAsyncCall(callback));	
	}

}
