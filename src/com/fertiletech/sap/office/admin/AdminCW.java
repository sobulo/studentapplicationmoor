package com.fertiletech.sap.office.admin;


import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class AdminCW extends CWEditParamsBase{

	public AdminCW() {
		super("Admins List", "Use this module to edit list of super admins in the database. Unique ID should be the email address of staff in question. " +
				" The other field should be used to add a brief description as to why the user is being granted admin privileges");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_ADMINS;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(AdminCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
