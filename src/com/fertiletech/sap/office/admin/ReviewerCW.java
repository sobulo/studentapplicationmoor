package com.fertiletech.sap.office.admin;

import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class ReviewerCW extends CWEditParamsBase{

	public ReviewerCW() {
		super("Reviewers List", "Add email address of staff you would like to" +
				" grant the ability to change the status of mortgage applications");
	}

	@Override
	protected boolean getShowValues() {
		return true;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_REVIEWER;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(ReviewerCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}

}
