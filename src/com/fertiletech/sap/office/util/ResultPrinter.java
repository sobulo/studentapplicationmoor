package com.fertiletech.sap.office.util;

/*
 * Copyright 2011 floreysoft GmbH (www.floreysoft.net)
 *
 * Written by Sergej Soller (ssoller@q-ric.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.sap.shared.DrivePickerUtils;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.floreysoft.gwt.picker.client.domain.ViewId;
import com.floreysoft.gwt.picker.client.domain.result.DocumentResult;
import com.floreysoft.gwt.picker.client.domain.result.MapResult;
import com.floreysoft.gwt.picker.client.domain.result.Thumbnail;
import com.floreysoft.gwt.picker.client.domain.result.ViewToken;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.UIObject;

public class ResultPrinter extends Composite {

	Button output;
	List<TableMessage> driveDocs = new ArrayList<TableMessage>();
	final PopupPanel previewPanel = new PopupPanel(true);
	final HTML descriptionContainer = new HTML();
	
	ResultPrinter() {
		output = new Button();
		output.setEnabled(false);
		clearOutput();
		initWidget(output);
		previewPanel.setGlassEnabled(true);
		previewPanel.setAnimationEnabled(true);
		previewPanel.add(new ScrollPanel(descriptionContainer));
		output.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String description = DrivePickerUtils.getAttachmentDescription(driveDocs);
				descriptionContainer.setHTML(description);
				previewPanel.center();
			}
		});
	}
	
	public void showPreview(TableMessage m, UIObject target)
	{
		String description = DrivePickerUtils.getAttachmentDescription(m, "PREVIEW");
		descriptionContainer.setHTML(description);
		previewPanel.showRelativeTo(target);
	}

	public String convertJSVal(String val) {
		if (val == null || val.equals("null") || val.equals("undefined"))
			return null;
		return val.trim();
	}

	public void setAttached(ViewToken token, DocumentResult result) {
		clearOutput();
		if (result.getDocs().length() == 0)
			return;
		HashMap<String, Integer> counts = new HashMap<String, Integer>();
		TableMessageHeader h = new TableMessageHeader();
		driveDocs.add(h);
		for (int i = 0; i < result.getDocs().length(); i++) {
			final DocumentResult.Document document = result.getDocs().get(i);
			TableMessage info = new TableMessage(7, 2, 2);
			info.setText(DrivePickerUtils.ATTACH_TITLE_IDX,
					convertJSVal(document.getName()));
			info.setText(DrivePickerUtils.ATTACH_PARENT_IDX,
					convertJSVal(document.getParentId()));
			info.setText(DrivePickerUtils.ATTACH_VIEW_IDX,
					convertJSVal(document.getEmbedUrl()));
			info.setText(DrivePickerUtils.ATTACH_EDIT_IDX,
					convertJSVal(document.getUrl()));
			info.setText(DrivePickerUtils.ATTACH_THUMB_IDX,
					getThumbUrl(document.getThumbnails(),document.getIconUrl()));
			info.setText(DrivePickerUtils.ATTACH_USR_IDX, ClientUtils.NPMBUserCookie.getCookie().getUserName());
			info.setDate(DrivePickerUtils.ATTACH_DATE_IDX,
					getLastEditDate(document.getLastEditedUtc()));
			info.setDate(DrivePickerUtils.ATTACH_LINKDATE_IDX, new Date());
			String docType = convertJSVal(document.getType()) == null ? "Other"
					: convertJSVal(document.getType());
			info.setText(DrivePickerUtils.ATTACH_TYPE_IDX, docType);
			String type = getViewID(token)==null?"Unknown":getViewID(token);
			if (!counts.containsKey(type))
				counts.put(type, new Integer(0));
			counts.put(type, counts.get(type) + 1);
			driveDocs.add(info);
		}
		StringBuilder caption = new StringBuilder("All/").append(result
				.getDocs().length());
		for (String type : counts.keySet())
			caption.append(" ").append(type).append("/")
					.append(counts.get(type));
		h.setCaption(caption.toString());
		output.setText("Preview " + result.getDocs().length() + " Files");
		output.setEnabled(true);
	}
	
	public void setAttached(ViewToken token, MapResult result, String title) {
		clearOutput();
		if (result.getDocs().length() == 0)
			return;
		TableMessageHeader h = new TableMessageHeader();
		driveDocs.add(h);
		for (int i = 0; i < result.getDocs().length(); i++) {
			final MapResult.Document document = result.getDocs().get(i);
			TableMessage info = new TableMessage(7, 2, 2);
			info.setText(DrivePickerUtils.ATTACH_TITLE_IDX, title);
			info.setText(DrivePickerUtils.ATTACH_VIEW_IDX,
					convertJSVal(document.getEmbedUrl()));
			info.setText(DrivePickerUtils.ATTACH_EDIT_IDX,
					convertJSVal(document.getUrl()));
			info.setText(DrivePickerUtils.ATTACH_THUMB_IDX,
					getThumbUrl(document.getThumbnails(), null));
			info.setText(DrivePickerUtils.ATTACH_TYPE_IDX,
					convertJSVal(getViewID(token)));
			info.setText(DrivePickerUtils.ATTACH_USR_IDX, ClientUtils.NPMBUserCookie.getCookie().getUserName());			
			info.setDate(DrivePickerUtils.ATTACH_LINKDATE_IDX, new Date());
			info.setNumber(DrivePickerUtils.ATTACH_LAT_IDX, document.getLatitude());
			info.setNumber(DrivePickerUtils.ATTACH_LONG_IDX, document.getLongitude());
			driveDocs.add(info);
		}
		h.setCaption("Map selected by NPMB staff");
		output.setText("Preview " + h.getCaption());
		output.setEnabled(true);
	}	

	private Date getLastEditDate(Long utc) {
		if (utc == null)
			return null;
		return new Date(utc);
	}

	private String getViewID(ViewToken token) {
		if (token == null)
			;
		ViewId id = token.getViewId();
		if (id == null)
			return convertJSVal(token.getNativeViewId().toLowerCase());
		else
			return convertJSVal(token.getViewId().toString());
	}

	private String getThumbUrl(JsArray<Thumbnail> thumbs, String defaultUrl) {
		String result = null;
		if (thumbs == null)
			return defaultUrl;
		for (int j = 0; j < thumbs.length(); j++) {
			final Thumbnail thumb = thumbs.get(j);
			result = convertJSVal(thumb.getUrl());
			if (result != null)
				break;
		}
		return result==null? defaultUrl : result;
	}
	
	public List<TableMessage> getValues()
	{
		return driveDocs;
	}

	public void clearOutput() {
		output.setText("Nothing Selected");
		output.setEnabled(false);
		driveDocs.clear();
	}
}
