package com.fertiletech.sap.office.util;

import com.fertiletech.sap.client.MyAsyncCallback;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;

public class ChangeFormStateHandler implements ClickHandler{

	Label formMessage, formState, display;
	PopupPanel p = new PopupPanel(true);
	TextBox message = new TextBox();
	final int MAX_CHARS = 120;
	ListBox stateList = new ListBox();
	public String loanID;
	public ChangeFormStateHandler(Label formSt, Label dp, Label formMsg)
	{
		this.formMessage = formMsg;
		this.formState = formSt;
		this.display = dp;
		message.setTitle("Briefly describe current state of the application. E.g. Application incomplete, passport photograph missing");
		message.setMaxLength(MAX_CHARS);
		for(WorkflowStateInstance state : WorkflowStateInstance.values())
			stateList.addItem(state.getDisplayString(), state.toString());
		final Button save = new Button("Change State");
		final Button cancel = new Button("Cancel");
		Grid container = new Grid(2, 2);
		container.setWidget(0, 0, stateList);
		container.setWidget(0, 1, message);
		container.setWidget(1, 1, save);
		container.setWidget(1,  0, cancel);
		p.setGlassEnabled(true);
		p.setStyleName("busy-PopupPanel");
		container.getCellFormatter().setHorizontalAlignment(1, 1, HasHorizontalAlignment.ALIGN_RIGHT);
		p.add(container);
		cancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				p.hide();
			}
		});
		final MyAsyncCallback<String> saveCallBack = new MyAsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Failed to change application state. <br/> Error: " + caught.getMessage());
				enableButtons(true);
			}

			@Override
			public void onSuccess(String result) {
				formMessage.setText(message.getText());
				formState.setText(stateList.getValue(stateList.getSelectedIndex()));
				display.setText((stateList.getItemText(stateList.getSelectedIndex())));
				PanelUtilities.infoBox.show("<b>State change successfull:</b><br/>" + result);
				enableButtons(true);
			}
			
			void enableButtons(boolean enabled)
			{
				stateList.setEnabled(enabled);
				message.setEnabled(enabled);
				save.setEnabled(enabled);
			}
			
			@Override
			protected void callService(AsyncCallback<String> cb) {
				enableButtons(false);
				p.hide();
				super.enableWarning(false);
				PanelUtilities.getLoanMktService().changeApplicationState(loanID, WorkflowStateInstance.valueOf(stateList.getValue(stateList.getSelectedIndex())),
						message.getValue(), cb);
			}
		};
		save.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(message.getText().trim().split(" ").length < 2)
				{
					PanelUtilities.errorBox.show("State description needs to be more than one word");
					return;
				}						
				saveCallBack.go("Changing state ...");
			}
		});
	}
	
	@Override
	public void onClick(ClickEvent event) {
		for(int i = 0; i < stateList.getItemCount(); i++)
		{
			if(stateList.getValue(i).equals(formState.getText()))
			{		
				stateList.setSelectedIndex(i);
				break;
			}
		}
		message.setValue(formMessage.getText());
		p.showRelativeTo(formMessage);
	}
}














