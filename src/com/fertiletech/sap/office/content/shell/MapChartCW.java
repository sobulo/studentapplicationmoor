package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.MapChartPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class MapChartCW extends ContentWidget{

	public MapChartCW() {
		super("Activity Map", "View where your customers are applying from (approximate location)." +
				" Specific locations attached by staff on the status page can also be viewed here.");
	}

	@Override
	public Widget onInitialize() {
		return new MapChartPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(MapChartCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});	
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
