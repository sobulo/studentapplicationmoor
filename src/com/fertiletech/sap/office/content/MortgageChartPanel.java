package com.fertiletech.sap.office.content;

import java.util.Date;

import com.fertiletech.sap.office.util.SearchDateBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.options.Options;

public abstract class MortgageChartPanel extends ResizeComposite{

	@UiField
	DockLayoutPanel container;
	@UiField
	SimplePanel footer;
	@UiField
	SimpleLayoutPanel content;
	@UiField
	SearchDateBox dateSearchPanel;
	protected ChartWidget<? extends Options> chart;
	Options opt;

	private static MortgageChartPanelUiBinder uiBinder = GWT
			.create(MortgageChartPanelUiBinder.class);

	interface MortgageChartPanelUiBinder extends
			UiBinder<Widget, MortgageChartPanel> {
	}

	public MortgageChartPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		dateSearchPanel.setEnabled(false);
		container.setWidgetSize(footer, 0);
		loadChartAPI();
	}
	

	/**
	 * This is the entry point method.
	 */
	private void loadChartAPI() {
		// Create the API Loader
		ChartLoader chartLoader = new ChartLoader(getChartType());
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				chart = getChart();
				content.add(chart);
				dateSearchPanel.setEnabled(true);
				dateSearchPanel.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						Date[] queryDates = dateSearchPanel.getSearchDates();
						if(queryDates == null) return;
						drawChart(queryDates[0], queryDates[1]);
					}
				});				
			}
		});
	}

	abstract protected ChartWidget<? extends Options> getChart();
	abstract protected ChartPackage getChartType();
	abstract protected void drawChart(final Date startDate, final Date endDate);

}
