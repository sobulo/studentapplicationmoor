package com.fertiletech.sap.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.sap.client.MortgageService;
import com.fertiletech.sap.server.downloads.GenericExcelDownload;
import com.fertiletech.sap.server.downloads.print.PrintApplicationForm;
import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.MortgageApplicationFormData;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.LoginRoles;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class MortgageServiceImpl extends RemoteServiceServlet implements
		MortgageService {
	private static final Logger log =
	        Logger.getLogger(MortgageServiceImpl.class.getName());	
	
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public String[] saveLoanApplicationData(HashMap<String, String> appData, ArrayList<HashMap<String, String>>[] supplementary,
			String loanKeyStr, boolean isSubmit)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		
		//actual
		String formEmail = appData.get(NPMBFormConstants.EMAIL);
		
		
		LoginRoles role = null;
		
		if(formEmail == null || formEmail.trim().length() == 0)
			throw new MissingEntitiesException("Invalid Arguments. Can't create loan/form data");
		String userEmail = LoginHelper.getLoggedInUser(this.getThreadLocalRequest());;
		if(userEmail != null)
		{
			role = LoginHelper.getRole(getThreadLocalRequest());
			if(role.equals(LoginRoles.ROLE_PUBLIC))
			{	
				if(!userEmail.equals(formEmail))
					throw( new MissingEntitiesException("Email mismatch: signed in as (" + userEmail + ") [VS.] form email(" + formEmail + ")"));
			}
			else
			{
				if(!LoginHelper.isSuperAdmin(userEmail, DTOConstants.APP_PARAM_EDITOR))
					throw new MissingEntitiesException("You do not have editor rights. Application edit/save failed");
				
			}
		}
		else
			userEmail = "anonymous user entry";
		Key<SalesLead> formSalesLeadKey = ObjectifyService.factory().stringToKey(loanKeyStr);

		MortgageApplicationFormData formData = EntityDAO.updateLoanLeadAndFormData(appData, supplementary, formSalesLeadKey, isSubmit, userEmail);
		LoginServiceImpl.setLastOpsLoan(formSalesLeadKey.getString(), getThreadLocalRequest());
		return getLoanIDArray(formSalesLeadKey, WorkflowStateInstance.valueOf(appData.get(NPMBFormConstants.ID_STATE)), mortgageFormEditable(formData));
	}
	
	private boolean mortgageFormEditable(MortgageApplicationFormData form)
	{
		return (isOpsUser() || !form.isSubmmited());
	}
	
	private boolean isOpsUser()
	{
		return !LoginHelper.getRole(getThreadLocalRequest()).equals(LoginRoles.ROLE_PUBLIC);
	}

	@Override
	public String[] startLoanApplication(HashMap<String, String> appData)
			throws DuplicateEntitiesException, MissingEntitiesException {
		String email = appData.get(NPMBFormConstants.EMAIL);
		if(email == null || email.trim().length() == 0)
			throw new MissingEntitiesException("Email required to start application");
		if(isOpsUser() && LoginHelper.getLoggedInUser(getThreadLocalRequest()).equalsIgnoreCase(email))
			throw new MissingEntitiesException("Staff not permitted to apply through public portal");
		if(isOpsUser() && !LoginHelper.isSuperAdmin(LoginHelper.getLoggedInUser(getThreadLocalRequest()), DTOConstants.APP_PARAM_EDITOR))
			throw new MissingEntitiesException("You do not have editor rights. Application edit/save failed");

		SalesLead salesLead= new SalesLead(email, EntityDAO.getAllocatedSalesID());
		appData.put(NPMBFormConstants.ID_STATE, salesLead.getCurrentState().toString());
		MortgageApplicationFormData data = EntityDAO.createLoanLeadAndFormData(salesLead, appData, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		LoginServiceImpl.setLastOpsLoan(salesLead.getKey().getString(), getThreadLocalRequest());
		return getLoanIDArray(salesLead.getKey());
	}
	
	private static String[] getLoanIDArray(Key<SalesLead> leadKey)
	{
		String[] result = new String[2];
		result[DTOConstants.LOAN_IDX] = leadKey.getString();
		result[DTOConstants.LOAN_ID_IDX] = String.valueOf(leadKey.getId());
		return result;
	}
	
	private static String[] getLoanIDArray(Key<SalesLead> leadKey, WorkflowStateInstance state, boolean isEditable)
	{
		String[] result = new String[4];
		result[DTOConstants.LOAN_IDX] = leadKey.getString();
		result[DTOConstants.LOAN_ID_IDX] = String.valueOf(leadKey.getId());
		result[DTOConstants.LOAN_STATE_IDX] = state.toString();
		result[DTOConstants.LOAN_EDIT_IDX] = String.valueOf(isEditable);
		return result;
	}

	@Override
	public HashMap<String, String> getStoredLoanApplication(String loanKeyStr) {
		LoginServiceImpl.setLastOpsLoan(loanKeyStr, getThreadLocalRequest());
		return EntityDAO.getLoanFormData(loanKeyStr, ObjectifyService.begin()).getFormData();		

	}

	@Override
	public TableMessage getLoanState(String loanID) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		LoginServiceImpl.setLastOpsLoan(leadKey.getString(), getThreadLocalRequest());
		return getLoanState(leadKey);
	}

	@Override
	public TableMessage getLoanState(long loanID) {
		Key<SalesLead> leadKey = getLoanKeyFromApplicationID(loanID);
		if(leadKey == null) return null;
		LoginServiceImpl.setLastOpsLoan(leadKey.getString(), getThreadLocalRequest());
		return getLoanState(leadKey);	}

	@Override
	public HashMap<String, String> getLoanApplicationWithLoanID(
			String loanKeyStr) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		log.warning("FETCHING DATA FOR: " + leadKey);
		MortgageApplicationFormData form = EntityDAO.getLoanFormData(ObjectifyService.begin(), leadKey);
		HashMap<String, String> result = form.getFormData();
		result.put(NPMBFormConstants.ID, loanKeyStr);
		result.put(NPMBFormConstants.ID_NUM, String.valueOf(leadKey.getId()));
		result.put(NPMBFormConstants.ID_EDIT, String.valueOf(mortgageFormEditable(form)));
		LoginServiceImpl.setLastOpsLoan(loanKeyStr, getThreadLocalRequest());
		return result;
	}
	
	public static TableMessage getLoanState(Key<SalesLead> leadKey)
	{
		Key<MortgageApplicationFormData> formKey = MortgageApplicationFormData.getFormKey(leadKey);
		Objectify ofy = ObjectifyService.begin();
		Map<Key<Object>, Object> loanObjects = ofy.get(formKey, leadKey);
		MortgageApplicationFormData formData = (MortgageApplicationFormData) loanObjects.get(formKey);
		SalesLead lead = (SalesLead) loanObjects.get(leadKey);
		//add a string,num,date val for each type of form associated with the applicant
		TableMessage result = new TableMessage(3, 2, 1);
		result.setMessageId(leadKey.getString());
		result.setText(DTOConstants.LOAN_KEY_IDX, formKey.getString());
		result.setNumber(DTOConstants.LOAN_KEY_IDX, formData.isSubmmited()?1:0);
		result.setDate(DTOConstants.LOAN_KEY_IDX, formData.getTimeStamp());
		result.setText(DTOConstants.LOAN_STATUS_IDX, lead.getCurrentState().toString());
		result.setNumber(DTOConstants.LOAN_STATUS_IDX, lead.getLeadID());
		result.setText(DTOConstants.LOAN_MESSAGE_IDX, lead.getDisplayMessaage());
		return result;		
	}
	

	private Key<SalesLead> getLoanKeyFromApplicationID(long id)
	{
		Objectify ofy = ObjectifyService.begin();
		List<Key<SalesLead>> result = ofy.query(SalesLead.class).filter("keyCopy =", id).listKeys();
		if(result.size() == 0)
			return null;
		else if(result.size() == 1)
			return result.get(0);
		else
		{
			String message = "";
			for(Key<SalesLead> k : result)
				message += k.toString() + " [and] ";
			throw new RuntimeException("Multiple keys found for " + id + " keys are: " + message);
		}
	}

	@Override
	public ArrayList<HashMap<String, String>>[] getSupplementaryData(
			String loanKeyStr) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		log.warning("FETCHING DATA FOR: " + leadKey);
		ArrayList<HashMap<String, String>>[] result = EntityDAO.getLoanFormData(ObjectifyService.begin(), leadKey).getSupplementaryData();
		LoginServiceImpl.setLastOpsLoan(leadKey.getString(), getThreadLocalRequest());
		return result;
	}

	@Override
	public String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header) {
		return GenericExcelDownload.getGenericExcelDownloadLink(data, header, getThreadLocalRequest());

	}

	@Override
	public List<TableMessage> getSaleLeads(Date startDate, Date endDate) {
		Query<SalesLead> saleLeads = EntityDAO.getSalesLeadsByDate(startDate, endDate);
		List<TableMessage> leadTable = EntityDAO.getConsumerLeadsAsTable(saleLeads);
		return leadTable;
	}

	@Override
	public String[] getApplicationFormDownloadLink(String appFormID) {
		return PrintApplicationForm.getApplicationFormLink(appFormID, getThreadLocalRequest());
	}

	@Override
	public List<TableMessage> getSalesLeadCollection(String loanID) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		String email = leadKey.getParent().getName();
		String loggedInEmail = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(isOpsUser())
			loggedInEmail = email;
		List<SalesLead> leads = EntityDAO.getConsumerLoanLeadViaEmail(ObjectifyService.begin(), loggedInEmail);
		LoginServiceImpl.setLastOpsLoan(loanID, getThreadLocalRequest());
		return EntityDAO.getConsumerLeadsAsPublicTable(leads);
	}
	
	@Override
	public String changeApplicationState(String loanStr, WorkflowStateInstance state,
			String message) throws MissingEntitiesException{
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanStr);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(!LoginHelper.isSuperAdmin(user, DTOConstants.APP_PARAM_REVIEWER))
			throw new MissingEntitiesException("You do not have reviewer rights. Application status change failed");
		return EntityDAO.changeState(leadKey, state, message, user);
	}

	@Override
	public HashMap<WorkflowStateInstance, Integer> getLeadAggregates(
			Date startDate, Date endDate) {
		return EntityConstants.getLeadAggregates(startDate, endDate);
	}	
}
