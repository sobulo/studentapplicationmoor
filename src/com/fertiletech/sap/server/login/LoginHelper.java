/**
 * 
 */
package com.fertiletech.sap.server.login;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.fertiletech.sap.server.OAuthLoginServiceImpl;
import com.fertiletech.sap.server.entities.ApplicationParameters;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.LoginRoles;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoginHelper {
	static Map<Key<ApplicationParameters>, ApplicationParameters> cachedTables;
	static Date cacheDate;
	private static final Logger log =
	        Logger.getLogger(LoginHelper.class.getName());
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}
	static void refreshCache()
	{
		log.warning("Cache Date: " + cacheDate);
		if(cacheDate == null || (new Date()).getTime() - cacheDate.getTime() > 1 * 60 * 1000)
		{
			log.warning("Refreshing cache");
			Key<ApplicationParameters>[] appKeys = new Key[3];
			String[] names = {DTOConstants.APP_PARAM_ADMINS, DTOConstants.APP_PARAM_EDITOR, DTOConstants.APP_PARAM_REVIEWER};
			for(int i = 0; i < names.length; i++)
				appKeys[i] = ApplicationParameters.getKey(names[i]);
			cachedTables = ObjectifyService.begin().get(appKeys);
			cacheDate = new Date();
		}		
	}

	public static boolean isSuperAdmin(String email, String type)
	{
		refreshCache();
		log.warning("Checking if " + email + " belongs to " + type);
		ApplicationParameters paramObj = cachedTables.get(ApplicationParameters.getKey(type));
		if(paramObj == null) return false;
		HashMap<String, String> adminTable = paramObj.getParams();
		log.warning("");
		for(String k : adminTable.keySet())
		{
			log.warning(k);
		}
		return adminTable.containsKey(email.toLowerCase().trim());
 	}
	
    public static LoginRoles getRole(HttpServletRequest req)
    {
    	String email = getLoggedInUser(req);
    	return getRole(email);
    }
    
    public static LoginRoles getRole(String email)
    {
    	if(email == null) email = "";
        if(email.endsWith(LoginConstants.COMPANY_DOMAIN) || email.endsWith(LoginConstants.FTBS_DOMAIN))
        {
        	if(isSuperAdmin(email, DTOConstants.APP_PARAM_ADMINS))
        		return LoginRoles.ROLE_SUPER_ADMIN;
        	else
        		return LoginRoles.ROLE_BANK_STAFF;
        }
        else
        	return	LoginRoles.ROLE_PUBLIC;        	
    }
    
    public static String getLoggedInUser(HttpServletRequest req)
    { 	    	
    	Object user = req.getSession().getAttribute(OAuthLoginServiceImpl.SESSION_EMAIL);
    	if(user == null) return null;
    	return (String) user;
    }
}
