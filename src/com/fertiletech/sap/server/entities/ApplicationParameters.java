/**
 * 
 */
package com.fertiletech.sap.server.entities;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class ApplicationParameters {
	
	@Id
	String myId;
	
	@Serialized
	HashMap<String, String> params;
	
	Text auditTrail;
	
	ApplicationParameters()
	{
		params = new HashMap<String, String>();
	}
	
	ApplicationParameters(String name)
	{
		this();
		this.myId = name;
	}
	
	ApplicationParameters(String name, HashMap<String, String> params)
	{
		this.myId = name;
		this.params = params;
	}
		
	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	public static Key<ApplicationParameters> getKey(String ID)
	{
		return new Key<ApplicationParameters>(ApplicationParameters.class, ID);
	}
	
	public Key<ApplicationParameters> getKey()
	{
		return getKey(myId);
	}
	
	public void addToAuditTrail(String comment, String user)
	{ 
		StringBuilder commentWrap;
		commentWrap = new StringBuilder();
		commentWrap.append("<div style='border:1px solid red; margin:5px;padding:5px;'>").
				append(comment).append("<p style='text-align:right; font-size: smaller;'>By ").append(user).
				append(" on ").append(new Date()).append("</p></div>");
		if(auditTrail != null)
			commentWrap.append(auditTrail.getValue());				
		auditTrail = new Text(commentWrap.toString());
	}
	
	public String getAuditTrail()
	{
		if(auditTrail == null) return "No history available";
		return auditTrail.getValue(); 
	}
}
