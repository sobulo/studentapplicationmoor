/**
 * 
 */
package com.fertiletech.sap.server.entities;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class MortgageComment implements Comparable<MortgageComment>{
	@Id
	Long key;
	
	Text comment;
	@Indexed Date dateUpdated;
	@Indexed Key<SalesLead> loanKey;
	boolean showPublic;
	String user;
	
	/**
	 * 
	 */
	public MortgageComment() {}
	
	/**
	 * @param key
	 * @param comment
	 * @param loanKey
	 * @param showPublic
	 * @param user
	 */
	MortgageComment(Text comment,
			Key<SalesLead> loanKey, boolean showPublic, String user) 
	{
		this.comment = comment;
		this.loanKey = loanKey;
		this.showPublic = showPublic;
		this.user = user;
	}
	
	public Text getComment() {
		return comment;
	}
	
	void setComment(Text comment) {
		this.comment = comment;
	}
	
	public boolean isShowPublic() {
		return showPublic;
	}
	
	void setShowPublic(boolean showPublic) {
		this.showPublic = showPublic;
	}
	
	public String getUser() {
		return user;
	}
	
	void setUser(String user) {
		this.user = user;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	public Key<MortgageComment> getKey()
	{
		return new Key<MortgageComment>(loanKey, MortgageComment.class, key);
	}
	
	public Key<SalesLead> getLoanKey()
	{
		return loanKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MortgageComment o) {
		int parentKeyComparison = loanKey.compareTo(o.loanKey);
		if(parentKeyComparison == 0)
			return this.dateUpdated.compareTo(o.dateUpdated);
		else
			return parentKeyComparison;
	}
}
