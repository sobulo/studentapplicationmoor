package com.fertiletech.sap.server.entities;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class SalesLeadIDHelper {
	@Id
	Long key;
	
	
	public SalesLeadIDHelper() {}
	
	@PrePersist
	void ensureNotSaved(){ throw new RuntimeException("Hey, I'm used for ID generation only"); }
}
