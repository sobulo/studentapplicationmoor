/**
 * 
 */
package com.fertiletech.sap.server.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class SalesLead{
	
	@Id
	long key;
	
	//customer fields -- potential index candidates, for now useful to have these without having to load forms
	String phoneNumber;
	String fullName;
	Integer olev1;
	Integer olev2;
	Double diplGpa;
	Double diplMaxGpa;
	String diplSchool;
	String displayMessage;

	
	
	WorkflowStateInstance currentState;
	
	//indexed fields
	@Indexed String bankLoanId;
	@Indexed Date dateCreated;
	@Indexed Long keyCopy;
	@Indexed Integer admYear;
	@Indexed boolean admFullTime;
	@Indexed boolean admND;  //store shortcode, display long
	@Indexed String admDepartment; //ditto
	
	//auto-populated fields
	Date dateUpdated; 
	String updateUser;	
	
	@Parent Key<ApplicantUniqueIdentifier> parentKey;
	
	public String getEmail(){
		return parentKey.getName();
	}
	

	public String getBankLoanId() {
		return bankLoanId;
	}

	void setBankLoanId(String bankLoanId) {
		this.bankLoanId = bankLoanId;
	}

	public Boolean getLoanApproved() {
		return currentState.isApprovedState();
	}

	public SalesLead() {} //empty constructor required by objectify
	
	public SalesLead(String email, long id)
	{
		this.parentKey = new Key<ApplicantUniqueIdentifier>(ApplicantUniqueIdentifier.class, email);
		this.currentState = WorkflowStateInstance.APPLICATION_STARTED;
		this.dateCreated = new Date(); 	
		this.admYear = DTOConstants.CURRENT_ADMISSION_YEAR;
		this.key = id;
	}
	
	void setDecisionFields(
			String phoneNumber,
			String fullName,
			Integer olev1,
			Integer olev2,
			Double diplGpa,
			Double diplMaxGpa,
			String diplSchool,
			String admFullTime,
			String admND,  //store shortcode, display long
			String admDepartment //ditto
	)
	{
		this.phoneNumber = phoneNumber;
		this.fullName = fullName;
		this.olev1 = olev1;
		this.olev2 = olev2;
		this.diplGpa = diplGpa;
		this.diplSchool = diplSchool;
		this.diplMaxGpa = diplMaxGpa;
		if(DTOConstants.FT.equals(admFullTime))
			this.admFullTime = true;
		else
			this.admFullTime = false;
		if(DTOConstants.ND.equals(admND))
			this.admND = true;
		else
			this.admND = false;
		this.admDepartment = admDepartment;
		//diplomaYear, diplname and olevName should go into hashmap form
	}
	
	SalesLead makeCopy(long id)
	{
		if(!allowNewSiblings()) throw new RuntimeException("Copying this lead is not allowed. Contact support for assistance");
		SalesLead copy = new SalesLead(parentKey.getName(), id);
		copy.setDecisionFields(phoneNumber, fullName, null, null, null, null, null, admFullTime? DTOConstants.FT : DTOConstants.PT,
				admND?DTOConstants.ND:DTOConstants.HND, null);
		return copy;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
			
	public String getFullName() {
		return fullName;
	}	
			
	public Double getDiplGpa() {
		return diplGpa;
	}


	public Double getDiplMaxGpa() {
		return diplMaxGpa;
	}


	public String getDiplSchool() {
		return diplSchool;
	}


	public boolean getAdmND() {
		return admND;
	}


	public String getAdmDepartment() {
		return admDepartment;
	}


	public Date getDateCreated() {
		return dateCreated;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public long getLeadID()
	{
		return key;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	String getUpdateUser() {
		return updateUser;
	}
	
	public Key<SalesLead> getKey()
	{
		return new Key<SalesLead>(parentKey, SalesLead.class, key);
	}

	public WorkflowStateInstance getCurrentState() {
		return currentState;
	}


	public void setCurrentState(WorkflowStateInstance state) {
		this.currentState = state;
	}
	
	public String getDisplayMessaage()
	{
		return displayMessage;
	}
	
	void setDisplayMessage(String msg)
	{
		displayMessage = msg;
	}




	
	public boolean allowNewSiblings()
	{
		Calendar oneYearAgo = GregorianCalendar.getInstance();
		oneYearAgo.roll(Calendar.YEAR, false);
		switch (currentState) 
		{
			case APPLICATION_APPROVED:
			case APPLICATION_CANCEL:
				return true;
			case APPLICATION_DENIED:
				return oneYearAgo.after(dateCreated);
			default:
				return false;
		}
	}
	
	@PrePersist
	void copyMyKeyForIndexing()
	{
		if(keyCopy == null)
			keyCopy = key;
	}
}