/**
 * 
 */
package com.fertiletech.sap.server.entities;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.fertiletech.sap.shared.WorkflowStateInstance;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class EntityConstants {
	public static final String GCS_HOST = "https://storage.googleapis.com/";
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
	
	public final static NumberFormat INT_FORMAT = NumberFormat.getIntegerInstance();
	
	public static HashMap<WorkflowStateInstance, Integer> getLeadAggregates(Date start, Date end)
	{
		Iterable<SalesLead> salesLeads = EntityDAO.getSalesLeadsByDate(start, end);
		HashMap<WorkflowStateInstance, Integer> aggregates = new HashMap<WorkflowStateInstance, Integer>();

		Integer stateCount = null;
		for(SalesLead lead : salesLeads)
		{
			stateCount = aggregates.get(lead.getCurrentState());
			if(stateCount == null)
				stateCount = 0;
			aggregates.put(lead.getCurrentState(), stateCount + 1);
		}		
		return aggregates;
	}
}
