/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.server.tasks;


import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.sap.server.ServiceImplUtilities;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.MortgageComment;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.messaging.MessagingController;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class CommonTasksHandler extends HttpServlet{
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}
	
    private static final Logger log =
            Logger.getLogger(CommonTasksHandler.class.getName());
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
    	log.warning("*******TASK EXECUTION BEGINNING!!!********");
        StringBuilder requestDetails = new StringBuilder(64);
        Enumeration<String> paramNames = req.getParameterNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Param Name: " + name.toString() + " Values: ").append(Arrays.toString(req.getParameterValues(name))).append("\n");
        }
        paramNames = req.getHeaderNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Header Name: " + name.toString() + " Values: ").append(req.getHeader(name)).append("\n");
        }

        //log.warning(requestDetails.toString());
        
        Objectify ofy = ObjectifyService.begin();

        String service = req.getParameter(TaskConstants.SERVICE_TYPE).trim();
        validateParamName(service, "No service specified");
        
        if(service.equals(TaskConstants.SERVICE_SEND_MESSAGE))
        {
        	log.warning("Running send message task");
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String controllerKeyStr = req.getParameter(TaskConstants.MSG_CONTROLLER_KEY_PARAM);
            String toAddress = req.getParameter(TaskConstants.TO_ADDR_PARAM);
            String[] messageBody = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
            String msgSubject = req.getParameter(TaskConstants.MSG_SUBJECT_PARAM);
            validateParamName(controllerKeyStr, "Value must be specified for message controller key");
            validateParamName(toAddress, "Value must be specified for recipeint address");
            validateParamName(messageBody, "Value must be specified for message body");
            validateParamName(msgSubject, "Value must be specified for subject field");


            Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerKeyStr);
             
        	MessagingController controller = ofy.find(controllerKey);
        	if(controller == null)
        		throw new IllegalStateException("Unable to find controller key");
        	boolean status = controller.sendMessage(toAddress, messageBody, msgSubject);
        	for(String s : messageBody)
        		log.warning("EMAIL: \n" + s);
        	//TODO modify log to reflect byte size of entire messageBody object rather than just text component
        	String msgSuffix = "message: " + messageBody[0].length() +
			" to " + toAddress + ". Controller Type: " + controller.getClass().getSimpleName();
        	if(status)
        		log.warning("Successfully sent " + msgSuffix);
        	else
        		log.severe("Error sending " + msgSuffix);
        }
        else if(service.equals(TaskConstants.SERVICE_CREATE_COMMENT))
        {
        	log.warning("running create comment task");
        	String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
        	Key<SalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
        	String[] comments = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
        	String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
        	validateParamName(loanKeyStr, "value most be specified for loan id");
        	validateParamName(comments, "at least 1 comment must be specified");
        	String loginUser = null;
        	try
        	{
        		validateParamName(updateUser, "ignore");
        		loginUser = updateUser;
        	}
        	catch(IllegalArgumentException EX)
        	{
        		loginUser = "map-alert";
        	}
        	
        	MortgageComment storedObj = EntityDAO.createComment(comments, false, leadKey, loginUser);
        	ServiceImplUtilities.logSystemUpdate(log, storedObj.getKey(), " comments associated with: " + storedObj.getLoanKey());
        }
        else if(service.equals(TaskConstants.SERVICE_SEND_MTG_MESSAGE))
        {
        	log.warning("running change loan state");
        	String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
        	Key<SalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
    		EntityDAO.sendLeadStateNotificationMessage(leadKey, "map-alert");
        }
    }

    private void validateParamName(String param, String message)
    {
        if(param == null || param.length() == 0)
            throw new IllegalArgumentException(message);
    }
    
    private void validateParamName(String[] params, String message)
    {
    	log.warning("PARAMS: " + params);
        if( params.length == 0 || params[0] == null || params[0].length() == 0)
            throw new IllegalArgumentException(message);
    }    
}
