package com.fertiletech.sap.server.downloads.print;

import java.util.HashMap;

import com.fertiletech.sap.shared.NPMBFormConstants;

public class PrintConstants {
	public static HashMap<String, String> PDFMappings = new HashMap<String, String>();
	static
	{
		PDFMappings.put(NPMBFormConstants.PRIOR_SESS_TYPE, "Student Type");
		PDFMappings.put(NPMBFormConstants.IT_START, "I.T. Period Hired");
		PDFMappings.put(NPMBFormConstants.ID_NUM, "Student Application ID");
		PDFMappings.put(NPMBFormConstants.DIPLOMA, "Diploma Type");
	}
}
