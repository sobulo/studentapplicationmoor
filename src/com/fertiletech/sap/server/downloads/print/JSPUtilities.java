/**
 * 
 */
package com.fertiletech.sap.server.downloads.print;

import java.text.DecimalFormat;
import java.util.HashMap;

import com.fertiletech.sap.server.entities.MortgageApplicationFormData;
import com.fertiletech.sap.shared.FormHTMLDisplayBuilder;
import com.fertiletech.sap.shared.NPMBFormConstants;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class JSPUtilities {


	  private static final String[] tensNames = {
	    "",
	    " ten",
	    " twenty",
	    " thirty",
	    " forty",
	    " fifty",
	    " sixty",
	    " seventy",
	    " eighty",
	    " ninety"
	  };

	  private static final String[] numNames = {
	    "",
	    " one",
	    " two",
	    " three",
	    " four",
	    " five",
	    " six",
	    " seven",
	    " eight",
	    " nine",
	    " ten",
	    " eleven",
	    " twelve",
	    " thirteen",
	    " fourteen",
	    " fifteen",
	    " sixteen",
	    " seventeen",
	    " eighteen",
	    " nineteen"
	  };

	  private static String convertLessThanOneThousand(int number) {
	    String soFar;

	    if (number % 100 < 20){
	      soFar = numNames[number % 100];
	      number /= 100;
	    }
	    else {
	      soFar = numNames[number % 10];
	      number /= 10;

	      soFar = tensNames[number % 10] + soFar;
	      number /= 10;
	    }
	    if (number == 0) return soFar;
	    return numNames[number] + " hundred" + soFar;
	  }


	  public static String convert(long number) {
	    // 0 to 999 999 999 999
	    if (number == 0) { return "zero"; }

	    String snumber = Long.toString(number);

	    // pad with "0"
	    String mask = "000000000000";
	    DecimalFormat df = new DecimalFormat(mask);
	    snumber = df.format(number);

	    // XXXnnnnnnnnn 
	    int billions = Integer.parseInt(snumber.substring(0,3));
	    // nnnXXXnnnnnn
	    int millions  = Integer.parseInt(snumber.substring(3,6)); 
	    // nnnnnnXXXnnn
	    int hundredThousands = Integer.parseInt(snumber.substring(6,9)); 
	    // nnnnnnnnnXXX
	    int thousands = Integer.parseInt(snumber.substring(9,12));    

	    String tradBillions;
	    switch (billions) {
	    case 0:
	      tradBillions = "";
	      break;
	    case 1 :
	      tradBillions = convertLessThanOneThousand(billions) 
	      + " billion ";
	      break;
	    default :
	      tradBillions = convertLessThanOneThousand(billions) 
	      + " billion ";
	    }
	    String result =  tradBillions;

	    String tradMillions;
	    switch (millions) {
	    case 0:
	      tradMillions = "";
	      break;
	    case 1 :
	      tradMillions = convertLessThanOneThousand(millions) 
	      + " million ";
	      break;
	    default :
	      tradMillions = convertLessThanOneThousand(millions) 
	      + " million ";
	    }
	    result =  result + tradMillions;

	    String tradHundredThousands;
	    switch (hundredThousands) {
	    case 0:
	      tradHundredThousands = "";
	      break;
	    case 1 :
	      tradHundredThousands = "one thousand ";
	      break;
	    default :
	      tradHundredThousands = convertLessThanOneThousand(hundredThousands) 
	      + " thousand ";
	    }
	    result =  result + tradHundredThousands;

	    String tradThousand;
	    tradThousand = convertLessThanOneThousand(thousands);
	    result =  result + tradThousand;

	    // remove extra spaces!
	    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	  }
	  
		public static FormHTMLDisplayBuilder loadFormDisplay(MortgageApplicationFormData formDataObj, boolean useDark) {		
			//check form ready for display only
			if(!formDataObj.isSubmmited())
				throw new RuntimeException("attempting to display an unsubmitted application");
			
			
			//build form
			FormHTMLDisplayBuilder display = new FormHTMLDisplayBuilder(useDark).formBegins();
			HashMap<String, String> formData = formDataObj.getFormData();
			//personal background info
			display.headerBegins().appendTextBody("Personal Information").headerEnds();
			display.appendTextBody("Name of Applicant", formData.get(NPMBFormConstants.SURNAME) + ", " + 
					formData.get(NPMBFormConstants.FIRST_NAME), true);
			
			display.lineBegins();
			display.appendTextBody("SEX", formData.get(NPMBFormConstants.GENDER));
			display.appendTextBody("Marital Status", formData.get(NPMBFormConstants.MARITAL_STATUS), false);
			display.appendTextBody("Date of Birth", formData.get(NPMBFormConstants.DATE_OF_BIRTH), false);
			display.lineEnds();
			
			display.lineBegins();
			display.appendTextBody("Personal email", formData.get(NPMBFormConstants.EMAIL), false);
			display.appendTextBody("Phone No", formData.get(NPMBFormConstants.TEL_NO), false);
			display.appendTextBody("Alternate No", formData.get(NPMBFormConstants.MOBILE_NO), false);
			display.lineEnds();
						
			return display;
		}
}
