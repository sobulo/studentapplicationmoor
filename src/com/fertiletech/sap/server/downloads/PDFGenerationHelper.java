/**
 * 
 */
package com.fertiletech.sap.server.downloads;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageFooter;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.pdfjet.Align;
import com.pdfjet.Cell;
import com.pdfjet.Font;
import com.pdfjet.RGB;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PDFGenerationHelper {
	private static final Logger log = Logger.getLogger(PDFGenerationHelper.class
			.getName());
	public static List<List<Cell>> convertTableMessageToPDFTable(List<TableMessage> tableData, Font headerFont, Font rowFont, HashSet<Integer> ignoreCols)
	{
		return convertTableMessageToPDFTable(tableData, headerFont, rowFont, null, ignoreCols, false);
	}
	
	public static List<List<Cell>> convertTableMessageToPDFTable(List<TableMessage> tableData, Font headerFont, Font rowFont, Font footerFont, HashSet<Integer> ignoreCols, boolean hideBorder)
	{
		if(ignoreCols == null) ignoreCols = new HashSet<Integer>();
		List<List<Cell>> result = new ArrayList<List<Cell>>(tableData.size());
		int textCursor, dateCursor, numberCursor;
		
		//setup header
		TableMessageHeader headerRow = (TableMessageHeader) tableData.get(0);
		
		//options
		boolean headerBG = false;
		for(int i : ignoreCols)
			if( i  < 0)
			{
				if(i == -1) headerBG = true;
				ignoreCols.remove(i);
			}
		int numOfCols = headerRow.getNumberOfHeaders() - ignoreCols.size();
		List<Cell> headerCell = new ArrayList<Cell>(numOfCols);		
		for(int i = 0; i < numOfCols; i++)
		{
			if(ignoreCols.contains(i)) continue;
			Cell h = new Cell(headerFont, headerRow.getText(i));
			
			if(headerBG)
			{
				h.setBgColor(RGB.DARK_GRAY);
				h.setFgColor(RGB.WHITE);
			}
			//h.setTextAlignment(Align.CENTER);
			if(hideBorder)
				h.setNoBorders();
			headerCell.add(h);
		}
		result.add(headerCell);
		
		//setup body
		for(int i = 1; i < tableData.size(); i++)
		{
			List<Cell> rowCell = new ArrayList<Cell>(numOfCols);
			TableMessage rowMessage = tableData.get(i);
			textCursor = dateCursor = numberCursor = 0;
			String dataVal = null;
			Double numVal = null;
			Date dateVal = null;
			Font cellFont = rowFont;
			double[] bgColor = null;
			if(rowMessage instanceof TableMessageFooter)
			{
				bgColor = RGB.LIGHT_GRAY;
				cellFont = footerFont;
			}
			for(int j = 0; j < numOfCols; j++)
			{
				TableMessageContent columnType = headerRow.getHeaderType(j);
				dataVal = "Invalid Column Type";
				if(columnType.equals(TableMessageContent.TEXT))
				{
					dataVal = rowMessage.getText(textCursor++);
					dataVal = dataVal==null?"":dataVal;
				}
				else if (columnType.equals(TableMessageContent.NUMBER))
				{
					numVal = rowMessage.getNumber(numberCursor++);
					dataVal = numVal == null?"":EntityConstants.NUMBER_FORMAT.format(numVal);
				}
				else if(columnType.equals(TableMessageContent.DATE))
				{
					dateVal = rowMessage.getDate(dateCursor++);
					dataVal = dateVal == null?"":EntityConstants.DATE_FORMAT.format(dateVal);
				}
				if(ignoreCols.contains(j)) continue;
				Cell colCell = new Cell(cellFont, dataVal);
				if(hideBorder)
					colCell.setNoBorders();
				if(bgColor != null)
					colCell.setBgColor(bgColor);
				rowCell.add(colCell);
				
			}
			result.add(rowCell);
		}
		return result;
	}
	
	public static List<List<Cell>> convertStackedTableMessageToPDFTable(List<TableMessage> tableData, Font headerFont, Font rowFont)
	{
		List<List<Cell>> result = new ArrayList<List<Cell>>(tableData.size());
		
		//setup body
		for(int i = 0; i < tableData.size(); i++)
		{
			TableMessage rowMessage = tableData.get(i);
			List<Cell> rowCell = new ArrayList<Cell>(rowMessage.getNumberOfTextFields());
			String dataVal = null;
			Font cellFont = rowFont;
			double[] bgColor = null;
			if(rowMessage instanceof TableMessageHeader)
			{
				bgColor = RGB.LIGHT_GRAY;
				cellFont = headerFont;
			}
			for(int j = 0; j < rowMessage.getNumberOfTextFields(); j++)
			{

				dataVal = rowMessage.getText(j);
				dataVal = dataVal==null?"":dataVal;
				Cell colCell = new Cell(cellFont, dataVal);
				int colspan;
				
				if(bgColor != null)
				{
					colCell.setBgColor(bgColor);
					colspan = Integer.valueOf(((TableMessageHeader) rowMessage).getCaption());
					colCell.setTextAlignment(Align.CENTER);
				}
				else
					colspan = (int) Math.round(rowMessage.getNumber(j));
				
					
				colCell.setColSpan(colspan);
				rowCell.add(colCell);
				for(int c = 1; c < colspan; c++)
					rowCell.add(new Cell(cellFont, ""));	
			}
			result.add(rowCell);
		}
		return result;
	}
	
	final static String LONG_STRING = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	public static List<TextLine> getPrintLines(String paragraph, Font f, double initialWidth, double maxWidth)
	{
		String stringWidthHelper = paragraph.length() > LONG_STRING.length()?paragraph:LONG_STRING;
		int padding = 6;
		int lineChars = f.getFitChars(stringWidthHelper, initialWidth) - padding;
		int maxChars = f.getFitChars(stringWidthHelper, maxWidth) - padding;

		String[] tokens = paragraph.split(" ");
		StringBuilder line = new StringBuilder();
		List<TextLine> result = new ArrayList<TextLine>();
		for(int i = 0; i < tokens.length; i++)
		{	
			if(line.length() + tokens[i].length() < lineChars)
				line.append(tokens[i]).append(" ");
			else
			{
				lineChars = maxChars;
				lineChars = f.getFitChars(paragraph, maxWidth);
				TextLine pdfLine = new TextLine(f, line.toString());
				line = new StringBuilder(tokens[i]).append(" ");
				result.add(pdfLine);
			}
		}
		result.add(new TextLine(f, line.toString()));
		return result;
	}
		
	public static String padValue(Font f, String val, double fieldWidth)
	{
		double width = f.stringWidth(val);
		double charWidth = f.stringWidth(" ");
		int paddedSpaces = (int) ((fieldWidth - width) / charWidth);
		return padRight(val, paddedSpaces + val.length());
	}
	
	public static String padRight(String s, int n) {
	     return String.format("%1$-" + n + "s", s);  
	}

	public static String padLeft(String s, int n) {
	    return String.format("%1$" + n + "s", s);  
	}	
}
