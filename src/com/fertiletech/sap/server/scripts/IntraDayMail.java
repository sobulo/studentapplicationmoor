/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.WorkflowStateInstance;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayMail extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayMail.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList = "9jedu-helpdesk@fcahptib.edu.ng";
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(cal.getTime());
		String endTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date());
		
		
		StringBuilder textMessage = new StringBuilder("Status report on this month's student applications, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Status report on student applications, start time: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("IT Name\tOps Name\tCounts\t");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'><tr><th>M.A.P. Status</th><th>Status Name</th><th>Counts</th></tr>");
		HashMap<WorkflowStateInstance, Integer> aggregates = EntityConstants.getLeadAggregates(cal.getTime(), new Date());
		for(WorkflowStateInstance st : WorkflowStateInstance.values())
		{
			int stateCount = (aggregates.get(st) == null)? 0 : aggregates.get(st);
			textMessage.append("\t").append(st.toString()).append("\t").append(st.getDisplayString()).append("\t").append(stateCount).append("\n");
			htmlMessage.append("<tr><td>").append(st.toString()).append("</td><td>").append(st.getDisplayString()).append("</td><td>").append(stateCount).append("</td></tr>");			
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList, messageContent, "M.A.P. Intra-Day Counts", false);
		out.println("<b>message sent</b><br/>");
	}
}
