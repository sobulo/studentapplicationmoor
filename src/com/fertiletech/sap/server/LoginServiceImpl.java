package com.fertiletech.sap.server;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.fertiletech.sap.client.LoginService;
import com.fertiletech.sap.server.entities.ApplicationParameters;
import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.InetImageBlob;
import com.fertiletech.sap.server.entities.MortgageAttachment;
import com.fertiletech.sap.server.entities.MortgageComment;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.DrivePickerUtils;
import com.fertiletech.sap.shared.FormHTMLDisplayBuilder;
import com.fertiletech.sap.shared.LoginRoles;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.fertiletech.sap.shared.oauth.OurException;
import com.fertiletech.sap.shared.oauth.SocialUser;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {

	private static final Logger log =
	        Logger.getLogger(LoginServiceImpl.class.getName());	
	
	  /**
	   * This is where backoff parameters are configured. Here it is aggressively retrying with
	   * backoff, up to 10 times but taking no more that 15 seconds total to do so.
	   */
	  private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	      .initialRetryDelayMillis(10)
	      .retryMaxAttempts(10)
	      .totalRetryPeriodMillis(15000)
	      .build());

	  /**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	  private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	
	BlobstoreService blobstore = BlobstoreServiceFactory.getBlobstoreService();

	public static String GCS_BUCKET_NAME = ""; 
	static
	{
		EntityDAO.registerClassesWithObjectify();
		try
		{
			GCS_BUCKET_NAME = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
		}
		catch(Exception Ex)
		{
			log.severe("Failed to auto initialize GCS Bucket Name, defaulting to string literal");
			log.severe(Ex.toString());
			GCS_BUCKET_NAME = "fcahpt-apply.appspot.com";
		}
	}
	private final static String LAST_OPS_LOAN_ID_REQUEST = "com.fertiletech.npmb.opslastloanidviewed";
	
	public static void setLastOpsLoan(String loandIDViewed, HttpServletRequest req)
	{
		LoginRoles role = LoginHelper.getRole(req);
		HttpSession sess = req.getSession();
		if(!role.equals(LoginRoles.ROLE_PUBLIC))
		{
			//log.warning("Last OPS ID Set to: " + LAST_OPS_LOAN_ID_REQUEST);
			sess.setAttribute(LAST_OPS_LOAN_ID_REQUEST, loandIDViewed);
		}
	}

	public static void stampNPMBInfo(SocialUser user, HttpSession sess) throws OurException
	{
		String email = user.getEmail();
		OAuthLoginServiceImpl.saveEmailToSession(email, sess);
    	user.role = LoginHelper.getRole(email);    	
		
    	if(user.role.equals(LoginRoles.ROLE_PUBLIC))
    	{
    		List<Key<SalesLead>> leadKeys = EntityDAO.getConsumerLoanLeadKeysViaEmail(ObjectifyService.begin(), email);
        	String[] IDs = new String[leadKeys.size()];
        	int count = 0;
        	
        	for(Key<SalesLead> lk : leadKeys)
        	{
        		IDs[count++] = lk.getString();
        	}
        	user.loanIDs = IDs;    		
    	}
    	else
    	{
			String[] loanIDs = {(String) sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST)};
        	if(!(sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST) == null))
    		{
    			if(loanIDs[0] != null)
    			{
    				user.loanIDs = loanIDs;
    				return;
    			}
    		}	    		
    	}
	}
	

	@Override
	public List<TableMessage> getLoanApplications(String email) 
	{
		Objectify ofy = ObjectifyService.begin();
		List<Key<SalesLead>> leadKeys = EntityDAO.getConsumerLoanLeadKeysViaEmail(ofy, email);
		Map<Key<SalesLead>, SalesLead> leads = ofy.get(leadKeys);
		return EntityDAO.getConsumerLeadsAsTable(leads.values());
	}

	@Override
	public List<TableMessage> getLoanApplications() {
		return getLoanApplications(LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}

	@Override
	public String loadActivityComments(String loanStr) {
		Key<SalesLead> key = ObjectifyService.factory().stringToKey(loanStr);
		List<MortgageComment> commentList = EntityDAO.getLoanComments(ObjectifyService.begin(), key);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder();
		result.formBegins();
		result.headerBegins().appendTextBody("<b>History</b>").headerEnds();
		boolean commentFound = false;
		
		for(MortgageComment c : commentList)
		{
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();			
			result.blankLine();
			result.sectionEnds();
		}
		if(!commentFound)
			result.lineBegins().appendTextBody("No comments found").lineEnds();
		result.formEnds();
		LoginServiceImpl.setLastOpsLoan(loanStr, getThreadLocalRequest());
		return result.toString();
	}

	@Override
	public List<TableMessage> getRecentActivityComments() {
		Objectify ofy = ObjectifyService.begin();
		List<MortgageComment> comments = ofy.query(MortgageComment.class).order("-dateUpdated").limit(100).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		for(MortgageComment c : comments)
		{
			TableMessage m = new TableMessage(3, 0, 1);
			m.setMessageId(c.getKey().getString());
			Text descriptiveComment = c.getComment();
			m.setText(0, c.getUser());
			m.setText(1, descriptiveComment == null?"Oops, no comment found, contact IT":descriptiveComment.getValue());
			m.setText(2, c.getLoanKey().getString());
			//log.warning(descriptiveComment.toString());
			m.setDate(0, c.getDateUpdated());
			result.add(m);
		}
		return result;
	}

	@Override
	public void saveActivityComment(String loanKeyStr, String text, boolean showPublic) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		EntityDAO.createComment(new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		LoginServiceImpl.setLastOpsLoan(leadKey.getString(), getThreadLocalRequest());
	}

	@Override
	public List<TableMessage> getAttachments(String loanID) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		return EntityDAO.getAttachmentsDTO(leadKey);
	}
	
	@Override
	public List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		return EntityDAO.getMapAttachments(startDate, endDate);
	}	

	@Override
	public String saveAttachments(String loanId, List<TableMessage> attachments) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanId);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		user = (user == null)? "anonymous user entry" : user; 
		List<MortgageAttachment> attch = EntityDAO.createAttachment(leadKey, attachments, user);
		return "Attached " + attch.size() + " new files for M.A.P. ID: " + leadKey.getId();
	}
	
	@Override
	public String saveMapAttachment(String loanId, Double lat, Double lng) {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(user == null) user = "anoymous user entry";
		TableMessage clientAttachment = DrivePickerUtils.getUserMapAttachment("User Edits", lat, lng, user);
		HttpServletRequest request = getThreadLocalRequest();
		Double altLat, altLng;
		altLng = altLat = null;
		String title="Untitled";
		try
		{
			String[] latlng = request.getHeader("X-AppEngine-CityLatLong").split(",");
			title = request.getHeader("X-AppEngine-Country") + "/" + request.getHeader("X-AppEngine-City"); 
			altLat = Double.valueOf(latlng[0]);
			altLng = Double.valueOf(latlng[1]);
		}
		catch(Exception e)
		{
			log.severe("Ignoring error while detecting lat/lng server side. Error: " + e.getMessage());
		}
		TableMessage serverAttachment = DrivePickerUtils.getUserMapAttachment("User Edits - " + title, altLat, altLng, user);
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(3);
		TableMessageHeader h = new TableMessageHeader();
		h.setCaption("User Edits - Location Detection");
		result.add(h);
		if(clientAttachment != null)
			result.add(clientAttachment);
		if(serverAttachment != null)
			result.add(serverAttachment);
		if(result.size() == 1)
			return null;
		return saveAttachments(loanId, result);
	}	

	@Override
	public String deleteAttachment(String attachID) {
		Key<MortgageAttachment> attachmentKey = ObjectifyService.factory().stringToKey(attachID);
		EntityDAO.removeAttachment(attachmentKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return "Removed 1 attachment for M.A.P. ID: " + attachmentKey.getParent().getId();
	}

	@Override
	public List<TableMessage> getUploads(String loanID) throws MissingEntitiesException{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "File Name", TableMessageContent.TEXT);
		h.setText(1, "Date", TableMessageContent.DATE);
		h.setMessageId(loanID);
		result.add(h);
		try {
			ListOptions listOpt = new ListOptions.Builder().setPrefix(loanID).build();
			ListResult items = gcsService.list(GCS_BUCKET_NAME, listOpt);
			while(items.hasNext())
			{
				ListItem record = items.next();
				if(record.isDirectory()) continue;
				TableMessage m = new TableMessage(2, 0, 1);
				m.setText(0, record.getName().replace(loanID, ""));
				m.setText(1, record.getName());
				m.setDate(0, record.getLastModified());
				m.setMessageId(EntityConstants.GCS_HOST + GCS_BUCKET_NAME + "/" + record.getName());
				//log.warning("Message id is: " + m.getMessageId());
				result.add(m);
			}
			//log.warning("Found Records: " + (result.size() - 1));
		} catch (IOException e) {
			throw new MissingEntitiesException("Unable to connect to cloud storage");
		}
		return result;
	}

	@Override
	public String getUploadUrl() {
		UploadOptions opts = UploadOptions.Builder.withGoogleStorageBucketName(GCS_BUCKET_NAME);
		String url = blobstore.createUploadUrl("/mydownload/success", opts);
		return url;
	}

	@Override
	public String deleteUpload(String uploadID) throws MissingEntitiesException {
		GcsFilename fileName = new GcsFilename(GCS_BUCKET_NAME, uploadID);
		boolean deleted = false;
		try {
			log.warning("Deleting: " + fileName);
			deleted = gcsService.delete(fileName);
			String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
			String[] loanParts = uploadID.split("/");
			if(loanParts.length > 1)
			{
				log.warning("deleted file");
				String comments[] = {"<div style='background-color:#c40069; color: white; text-align:center;'>Upload Deleted</div>", "Filename: " + loanParts[1]};
				TaskQueueHelper.scheduleCreateComment(comments, loanParts[0], user ==null?"anoymous":user);
			}
			else
				log.warning("Nothing deleted");
		} catch (IOException e) {
			throw new MissingEntitiesException("Failed to delete " + fileName.toString() 
					+ " from cloud storage. Error was: " + e.getMessage());
		}
		
		return deleted?  ("Successfully deleted " + fileName.getObjectName()) : ("Failed to delete: " + fileName.getObjectName()); 
	}
	
	@Override
	public List<TableMessage> getApplicationParameter(String ID) {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		HashMap<String, String> params = appObj.getParams();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "Unique Identifier", TableMessageContent.TEXT);
		h.setText(1, "Description", TableMessageContent.TEXT);
		h.setMessageId(appObj.getKey().getString());
		h.setCaption(appObj.getAuditTrail());
		result.add(h);
		for(String s : params.keySet())
		{
			TableMessage m = new TableMessage(2, 0, 0);
			m.setText(0, s);
			m.setText(1, params.get(s));
			m.setMessageId(s);
			result.add(m);
		}
		Collections.sort(result);
		return result;
	}

	@Override
	public String saveApplicationParameter(String ID, HashMap<String, String> val) throws MissingEntitiesException {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		String updateUser = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		return updateApplicationParameters(updateUser, pk, val);
	}
	public static String updateApplicationParameters(String userID,
			Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				throw new MissingEntitiesException("Parameter does not exist, unable to update" + key);
 
			HashMap<String, String> oldParameters = paramsObject.getParams();
			paramsObject.setParams(parameters);
			StringBuffer diffs = new StringBuffer("<ol>");
			for(String o : oldParameters.keySet())
			{
				if(parameters.containsKey(o))
				{
					if(!parameters.get(o).equalsIgnoreCase(oldParameters.get(o)))
						diffs.append("<li>Description for " + o + " changed from: <font color='red'>[" + oldParameters.get(o) + "]</font> to: <font color='purple'>[" + parameters.get(o) + "]</font><li>");
				}
				else
					diffs.append("<li>Entry for " + o +" <b style='color:red'>REMOVED</b>. Its description at time of removal was: [" + oldParameters.get(o) + "]");
					
			}
			for(String o : parameters.keySet())
			{
				if(!oldParameters.containsKey(o))
					diffs.append("<li>Entry for " + o + " <b style='color:purple'>ADDED</b>. Its description is: [" + parameters.get(o) + "]</li>");
			}
			paramsObject.addToAuditTrail(diffs.toString(), userID);
			ofy.put(paramsObject);
			ofy.getTxn().commit();
			return paramsObject.getAuditTrail(); 

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	@Override
	public String getImageUploadHistory(String imageID) {
		return ObjectifyService.begin().get(InetImageBlob.getKey(imageID)).getAuditTrail();
	}
	
}
